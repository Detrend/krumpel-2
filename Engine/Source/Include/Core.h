#pragma once

#include <chrono>

#include "Basics.h"
#include "ResourceSystem.h"
#include "Logger.h"

#include "Renderer.h"
#include "Mesh.h"
#include "Texture.h"
#include "Framebuffer.h"
#include "Shader.h"

#include "Scene.h"

#include "iEditor.h"
#include "Input.h"

#include "Glew.h"


KR_BEGIN

class Engine;
class cWindow;
class cMouse;
class cKeyboard;
class cEvent;

/*
    -Core
        -Global memory pool
        -list of Scenes
            -Camera
            -List of objects		[solid]
            -List of entities		[updates]
            -Update entities(ticks)
        -list of renderers
                -render(scene, camera, framebuffer)
        -Window
        -Mouse
        -Keyboards
        -list of controllers
*/

// core should not interact with opengl/directx directly

// Type of engine startup mode
enum class EngineMode
{
    Editor = 0,
    Game = 1
};


// Core initialization arguments
struct sCoreInitArguments
{
    bool            FullscreenOn            = false;
    bool            VisibleConsole          = true;
    bool            DevtoolsEnabled         = true;
    int				WindowWidth             = 1280;
    int				WindowHeight            = 720;
    LoggerSeverityLevel	LoggerSeverityLevel = LoggerSeverityLevel::ReportEverything;     // What warnings should we receive
    EngineMode      Mode                    = EngineMode::Editor;               // Prepare the core to run game or editor
    const char*		LoggerOutput            = nullptr;                          // Path to output file. If null then output to console
};

class cCore
{
private:
    std::unique_ptr<cWindow>	mMainWindow;
    std::unique_ptr<cKeyboard>	mKeyboard;
    std::unique_ptr<cMouse>		mMouse;

    std::unique_ptr<cFramebuffer>	mMainFramebuffer;
    std::unique_ptr<cRenderer3D>    mRenderer3D;
    std::unique_ptr<cScene>			mScene;

    std::unique_ptr<iEditor>        mEditor;
    bool							mIsInitialized;

private:
    // --- MAIN SETUP - called by Init function ---
    // Sets up the window to render into
    Result<void> SetupWindow(const sCoreInitArguments& CoreSettings);					

    // Sets up rendering engine
    Result<void> SetupRendering();
    
    // --- INTERNAL SETUP - called only by main setup functions ---
    // Sets up the extensions, like vsync turn-off
    Result<void> SetupExtensions();

    // Checks if the extension is supported
    static bool WGLExtensionIsSupported(const char* ExtensionName);

    // -- CLEANUP --
    // Officialy terminates ImGui

    // -- RUNTIME --
    void ProcessInput(InputData& InputData);

    void Update(std::chrono::system_clock::duration DeltaTime, InputData Input);

    void Render();

public:
    explicit cCore();

    ~cCore();

    Result<void> Init();

    Result<void> Init(const sCoreInitArguments& CoreArguments);

    Result<void> Terminate();

    iEditor* GetEditor();

    void Run();

friend class LevelEditor;
};

KR_END