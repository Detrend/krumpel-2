#pragma once

#include <Windows.h>
#include <bitset>
#include <string>

#include "Basics.h"


KR_BEGIN

LRESULT CALLBACK DefaultWindowProcedure(HWND, UINT, WPARAM, LPARAM);

// Number of keyboard keys we should store state of
constexpr uint32 KEYBOARD_KEYS_COUNT = 256;
using KeyCode = uint8;

class cKeyboard
{
    protected:
        std::bitset<KEYBOARD_KEYS_COUNT> mCheckedKeys;
        std::bitset<KEYBOARD_KEYS_COUNT> mPressedKeys;
        std::bitset<KEYBOARD_KEYS_COUNT> mReleasedKeys;

        void SetChecked(KeyCode key, bool state);
        void SetPressed(KeyCode key, bool state);
        void SetReleased(KeyCode key, bool state);

    public:
        cKeyboard();
        // Returns true if key with certain 'Code' is checked
        bool IsChecked(KeyCode Code);
        // Returns true if key with certain 'Code' was pressed
        bool IsPressed(KeyCode Code);
        // Returns true if key with certain 'Code' was released
        bool IsReleased(KeyCode Code);
        // Clears state of all keys.
        // Must be called before every new update of the keyboard to clear old states.
        void Clear();

    friend LRESULT CALLBACK DefaultWindowProcedure(HWND, UINT, WPARAM, LPARAM);
    friend void HandleKeyboardEvent(UINT, cKeyboard*, WPARAM, LPARAM);
};

void HandleKeyboardEvent(UINT Msg, cKeyboard* Keyboard, WPARAM Wparam, LPARAM Lparam);

KR_END