#include <iostream>
#include <filesystem>

int main()
{
    std::filesystem::path Path = "hello/this/is/random/path.txt";
    Path = std::filesystem::absolute(Path);

    std::cout << sizeof(std::filesystem::path) << std::endl;

    std::wcout << Path.native().c_str() << std::endl;
}