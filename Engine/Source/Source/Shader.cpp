#include <fstream>
#include <string>
#include <filesystem>
#include <array>

#include "Shader.h"
#include "OpenGLError.h"
#include <glm/gtc/type_ptr.hpp>

KR_BEGIN

GLuint cShaderComponent::GetHandle() const
{
	return mID;
}

bool cShaderComponent::IsCompiled() const
{
	return mCompiled;
}

cShaderComponent::~cShaderComponent()
{
	if (IsAlive()) {
		DestroyShaderComponent();
	}
}

void cShaderComponent::CreateShaderComponent()
{
	if (mID == 0) {
		GLenum type;
		switch (mShaderType) {
		case SHADER_VERTEX:
			type = GL_VERTEX_SHADER;
			break;
		case SHADER_GEOMETRY:
			type = GL_GEOMETRY_SHADER;
			break;
		case SHADER_FRAGMENT:
			type = GL_FRAGMENT_SHADER;
			break;
		default:
			KR_BREAKPOINT;
			break;
		}
		mID = glCreateShader(type);
	}
}

void cShaderComponent::DestroyShaderComponent()
{
	glDeleteShader(mID);
	mID = 0;
}

bool cShaderComponent::IsAlive()
{
	return (mID != 0);
}

static uint GetShaderFileSize(std::ifstream& aFile)
{
    // Get size of the file and reserve enought space for its content
	std::streampos OriginalPosition, FileBegin, FileEnd;
	OriginalPosition = aFile.tellg();
	aFile.seekg(0, aFile.beg);
	FileBegin = aFile.tellg();
	aFile.seekg(0, aFile.end);
	FileEnd = aFile.tellg();
	aFile.seekg(OriginalPosition, aFile.beg);
	uint Filesize = static_cast<uint>(FileEnd - FileBegin);
	return Filesize;
}

Result<> cShaderComponent::CompileFromFile(const std::filesystem::path& aPath)
{
	std::string ShaderSource;	
	std::ifstream File;

	File.open(aPath, std::ios::in);
	if (!File.is_open()) {
		Logger.Error("File with shader code can not be opened or is missing.", "Shader compiler");
		return RESULT_NOT_FOUND;
	}

	uint Filesize = GetShaderFileSize(File);
	ShaderSource.reserve(Filesize);

	std::string Line;

	// Read the whole file and then close it
	while (std::getline(File, Line)) {
		Line.append("\n");
		ShaderSource.append(Line);
	}
	File.close();

	if (!IsAlive())
		CreateShaderComponent();

	constexpr uint BufferSize = 1024u;
	char infoLog[BufferSize];
	GLint success;

	const char* src = ShaderSource.c_str();
	glShaderSource(mID, 1, &src, NULL);

	glCompileShader(mID);
	glGetShaderiv(mID, GL_COMPILE_STATUS, &success);
	if (success == GL_FALSE) {
		glGetShaderInfoLog(mID, BufferSize, NULL, infoLog);
		Logger.Error(infoLog, "Shader compiler");
		return {RESULT_FAIL};
	}
	
	mCompiled = true;
	return {RESULT_OK};
}

cShader::cShader()
	:	Fragment(SHADER_FRAGMENT), 
		Vertex(SHADER_VERTEX), 
		Geometry(SHADER_GEOMETRY),
		mID(0),
		mLinked(false) {}

cShader::~cShader()
{
	if (IsAlive()) {
		DestroyProgram();
	}
}

bool cShader::IsAlive()
{
	return (mID != 0);
}

void cShader::CreateProgram()
{
	mID = glCreateProgram();
}

void cShader::DestroyProgram()
{
	glDeleteProgram(mID);
	mID = 0;
}

GLuint cShader::GetHandle() const
{
	return mID;
}

Result<> cShader::Link() {

	if (!IsAlive())
	{
		CreateProgram();
	}

	// if none of the shader attachments was compiled
	if (!Vertex.IsCompiled() and !Fragment.IsCompiled() and !Geometry.IsCompiled()) {
		Logger.Error("Failed to link shader, none of shader attachments is compiled", "Shader linker");
		return RESULT_FAIL;
	}
	
	GLint success;
	constexpr uint BufferSize = 1024u;
	char infoLog[BufferSize];

	if (Vertex.IsCompiled()) {
		glAttachShader(mID, Vertex.GetHandle());
	}
	if (Fragment.IsCompiled()) {
		glAttachShader(mID, Fragment.GetHandle());
	}
	if (Geometry.IsCompiled()) {
		glAttachShader(mID, Geometry.GetHandle());
	}

	glLinkProgram(mID);
	glGetProgramiv(mID, GL_LINK_STATUS, &success);
	if (success == GL_FALSE)
	{
		glGetProgramInfoLog(mID, BufferSize, NULL, infoLog);
		Logger.Error(infoLog, "Shader linker");
		return RESULT_FAIL;
	}

	mLinked = true;
	Logger.Notify("Shader linked successfully", "Shader linker");
	return RESULT_OK;
}

Result<> cShader::LinkFromFile(const std::filesystem::path& aPath)
{
	std::fstream File;
	File.open(aPath);

	if (!File.is_open())
	{
		return eResult::RESULT_NOT_FOUND;
	}

	constexpr const char* FRAGMENT_SIGNATURE = "#include_fragment ";
	constexpr const char* VERTEX_SIGNATURE = "#include_vertex ";
	constexpr const char* GEOMETRY_SIGNATURE = "#include_geometry ";

	std::string FragmentName;
	std::string VertexName;
	std::string GeometryName;

	// Read the whole file
	std::string Line;
	while (std::getline(File, Line))
	{
		std::size_t Pos;
		if ((Pos = Line.find(FRAGMENT_SIGNATURE)) != std::string::npos)
		{
			FragmentName = Line.substr(Pos + std::strlen(FRAGMENT_SIGNATURE));
		}
		else if ((Pos = Line.find(VERTEX_SIGNATURE)) != std::string::npos)
		{
			VertexName = Line.substr(Pos + std::strlen(VERTEX_SIGNATURE));
		}
		else if ((Pos = Line.find(GEOMETRY_SIGNATURE)) != std::string::npos)
		{
			GeometryName = Line.substr(Pos + std::strlen(GEOMETRY_SIGNATURE));
		}
		else
		{
			// Parse shader requirements
			for (auto&& Pair : PossibleShaderRequirements)
			{
				if ((Pos = Line.find(std::get<0>(Pair))) != std::string::npos)
				{
                    std::string RestOfTheLine = Line.substr(Pos + std::get<0>(Pair).length());
					int SlotNumber;
					try
					{
                        SlotNumber = std::stoi(RestOfTheLine);
					}
					catch (const std::invalid_argument&)
					{
						// Skip this requirement
						Logger.Warning("Failed to deduce shader requirement slot ID", "Shader parser");
						break;
					}
					
					auto& Requirement = mRequirements.emplace_back();
					Requirement.Requirement = std::get<1>(Pair);
					Requirement.Slot = SlotNumber;
					break;
				}
			}
		}
	}

	File.close();

	if (FragmentName.empty() or VertexName.empty())
	{
		return eResult::RESULT_FAIL;
	}

	std::filesystem::path FragmentPath = aPath;
	std::filesystem::path VertexPath = aPath;
	std::filesystem::path GeometryPath = aPath;
	FragmentPath.replace_filename(FragmentName);
	VertexPath.replace_filename(VertexName);
	GeometryPath.replace_filename(GeometryName);

	if (Failed(Fragment.CompileFromFile(FragmentPath)))
	{
		return eResult::RESULT_FAIL;
	}
	if (Failed(Vertex.CompileFromFile(VertexPath)))
	{
		return eResult::RESULT_FAIL;
	}

	if (!GeometryName.empty())
	{
		if (Failed(Geometry.CompileFromFile(GeometryPath)))
		{
			return eResult::RESULT_FAIL;
		}
	}

	if (Failed(Link()))
	{
		return eResult::RESULT_FAIL;
	}
	else
	{
		return eResult::RESULT_OK;
	}
}

void cShader::Bind() const
{
	glUseProgram(mID);
}

bool cShader::IsNull() const
{
	return (this == &NullShader);
}

const std::list<sRequirementSlotPair>& cShader::GetRequirements() const
{
	return mRequirements;
}

bool cShader::IsLinked() const 
{
	return mLinked;
}

KR_END