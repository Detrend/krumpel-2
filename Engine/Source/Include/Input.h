#pragma once

#include <bitset>

#include "Basics.h"
#include "Mouse.h"
#include "Keyboard.h"

KR_BEGIN

struct InputData
{
    // Mouse data
    struct {
        int64 XPos;         // X-pos of the mouse relative to the screen
        int64 YPos;         // Y-pos of the mouse relative to the screen
        int64 XChange;      // X-change in mouse coord 
        int64 YChange;      // Y-change in mouse coord 
        int64 Scroll;       // Mouse scroll delta
        std::bitset<(int)MouseButton::Count> Checked;       // Which mouse buttons are held
        std::bitset<(int)MouseButton::Count> Pressed;       // Which mouse buttons were pressed
        std::bitset<(int)MouseButton::Count> Released;      // Which mouse buttons were released
    } Mouse;

    // Keyboard data
    struct {
        std::bitset<KEYBOARD_KEYS_COUNT> Checked;           // Keys that are held
        std::bitset<KEYBOARD_KEYS_COUNT> Pressed;           // Keys that were just pressed
        std::bitset<KEYBOARD_KEYS_COUNT> Released;          // Keys that were just released
    } Keyboard;
};

KR_END
