#define _CRT_SECURE_NO_WARNINGS
#include <Windows.h>
#include <hidusage.h>		// Winapi raw mouse macros
#include <algorithm>
#include <chrono>
#include <cstdio>
#include <string>

#include "Glew.h"

#include "Basics.h"

#include "Core.h"
#include "Window.h"
#include "DefProc.h"
#include "Mouse.h"
#include "Keyboard.h"
#include "EmptyEditor.h"
#include "LevelEditor.h"

#include "Generator.h"

KR_BEGIN

cCore::cCore()
    : mKeyboard(nullptr),
      mMainWindow(nullptr),
      mMouse(nullptr),
      mIsInitialized(false)
      {}

cCore::~cCore()
{
    
}

Result<void> cCore::SetupRendering()
{
    wglMakeCurrent(mMainWindow->GetDeviceContext(), mMainWindow->GetRenderingContext());
    mRenderer3D = std::make_unique<cRenderer3D>();
    mMainFramebuffer = std::make_unique<cFramebuffer>();
    mMainFramebuffer->Create(mMainWindow->GetWidth(), mMainWindow->GetHeight(), 4, 1);

    PossibleShaderRequirements.push_back({"#require_uniform_mat4 camera_to_projection ", REQUIRE_UNIFORM_MAT4_PROJECTION});
    PossibleShaderRequirements.push_back({"#require_uniform_mat4 world_to_camera ", REQUIRE_UNIFORM_MAT4_WORLD});
    PossibleShaderRequirements.push_back({"#require_uniform_mat4 object_to_world ", REQUIRE_UNIFORM_MAT4_OBJECT});
    PossibleShaderRequirements.push_back({"#require_uniform_sampler2D main_texture ", REQUIRE_UNIFORM_SAMPLER2D_MAIN_TEXTURE});

    return {RESULT_OK};
}

void cCore::ProcessInput(InputData& aInputData)
{
    if (mMouse != nullptr)
    {
        if (!mEditor->BlockMouse())
        {
            aInputData.Mouse.XPos = mMouse->GetMouseX();
            aInputData.Mouse.YPos = mMouse->GetMouseY();
            aInputData.Mouse.XChange = static_cast<int>(mMouse->GetChangeX());
            aInputData.Mouse.YChange = static_cast<int>(mMouse->GetChangeY());
            for (int i = 0; i < (int)MouseButton::Count; i++) {
                aInputData.Mouse.Checked[i] = mMouse->IsChecked((MouseButton)i);
                aInputData.Mouse.Pressed[i] = mMouse->IsPressed((MouseButton)i);
                aInputData.Mouse.Released[i] = mMouse->IsReleased((MouseButton)i);
            }
        }
    }

    if (mKeyboard != nullptr)
    {
        if (!mEditor->BlockKeyboard())
        {
            for (int i = 0; i < KEYBOARD_KEYS_COUNT; i++) {
                aInputData.Keyboard.Checked[i] = mKeyboard->IsChecked(i);
                aInputData.Keyboard.Pressed[i] = mKeyboard->IsPressed(i);
                aInputData.Keyboard.Released[i] = mKeyboard->IsReleased(i);
            }
        }
    }
}

void cCore::Update(std::chrono::system_clock::duration DeltaTime, InputData Input)
{
    long long Micros = std::chrono::duration_cast<std::chrono::microseconds>(DeltaTime).count();
    long long AverageMicros = 16'000ll;
    float Delta = (float)Micros / AverageMicros;
    auto& Camera = mScene->GetCamera();
    float FlySpeed = 1.0f;

    // Check for mouse buttons
    if (Input.Mouse.Checked[(std::size_t)MouseButton::Right])
    {
        glm::mat3 Rotation = Camera.GetRotationMatrix();
        float MouseHorizontalChange = Input.Mouse.XChange / -1600.0f;
        float MouseVerticalChange = Input.Mouse.YChange / -1600.0f;

        const glm::vec3 UpVector = glm::vec3(0.0f, 1.0f, 0.0f);
        glm::mat3&& HorizontalRotation = glm::mat3(glm::rotate(glm::mat4(1.0), MouseHorizontalChange, glm::vec3(0.0f, 1.0f, 0.0f)));
        Rotation = HorizontalRotation * Rotation;
        const glm::vec3& RightVector = Rotation[0];
        glm::mat3&& VerticalRotation = glm::mat3(glm::rotate(glm::mat4(1.0), MouseVerticalChange, RightVector));
        Rotation = VerticalRotation * Rotation;

        Camera.SetRotationMatrix(Rotation);
        mMouse->SetCursorPosition(*mMainWindow.get(), mMainWindow->GetWidth() / 2, mMainWindow->GetHeight() / 2);
    }


    // Update flying speed
    FlySpeed = Input.Keyboard.Checked[VK_SHIFT] ? 2.0f : 1.0f;

    if (Input.Keyboard.Checked['A'])
        Camera.SetPosition(Camera.GetPosition() - Camera.GetRotationMatrix()[0] * 0.01f * FlySpeed * Delta);
    if (Input.Keyboard.Checked['D'])
        Camera.SetPosition(Camera.GetPosition() + Camera.GetRotationMatrix()[0] * 0.01f * FlySpeed * Delta);
    if (Input.Keyboard.Checked['W'])
        Camera.SetPosition(Camera.GetPosition() - Camera.GetRotationMatrix()[2] * 0.01f * FlySpeed * Delta);
    if (Input.Keyboard.Checked['S'])
        Camera.SetPosition(Camera.GetPosition() + Camera.GetRotationMatrix()[2] * 0.01f * FlySpeed * Delta);
    if (Input.Keyboard.Checked['E'])
        Camera.SetPosition(Camera.GetPosition() + glm::vec3(0.0f, 1.0f, 0.0f) * 0.01f * FlySpeed * Delta);
    if (Input.Keyboard.Checked['Q'])
        Camera.SetPosition(Camera.GetPosition() - glm::vec3(0.0f, 1.0f, 0.0f) * 0.01f * FlySpeed * Delta);

    if (Input.Keyboard.Checked[VK_ESCAPE])
        mMainWindow->Close();

    mEditor->Update();
}

void cCore::Render()
{
    mMainWindow->BindContext();
    mMainWindow->SetVsync(false);
    
    glEnable(GL_DEPTH_TEST);
    mMainFramebuffer->Bind();
    glClearColor(0.9f, 0.2f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Render scene
    if (mScene)
        mRenderer3D->Render(*mScene);

    // Render the editor GUI
    if (mEditor)
        mEditor->Render();

    glBindFramebuffer(GL_READ_FRAMEBUFFER, mMainFramebuffer->GetFramebufferHandle());
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBlitFramebuffer(0, 0, mMainWindow->GetWidth(), mMainWindow->GetHeight(), 0, 0,
        mMainWindow->GetWidth(), mMainWindow->GetHeight(), GL_COLOR_BUFFER_BIT, GL_LINEAR);

    glFlush();
    mMainWindow->Swap();
}

Result<void> cCore::SetupWindow(const sCoreInitArguments& aSetupSettings)
{
    mMainWindow = std::make_unique<cWindow>();
    auto WindowInitialization = mMainWindow->Init(this, L"Krumpel Sandbox Game",
        aSetupSettings.WindowWidth, aSetupSettings.WindowHeight, aSetupSettings.FullscreenOn);
    if (Failed(WindowInitialization))
        return {RESULT_INIT_FAIL};

    // Setup raw input for mouse
    RAWINPUTDEVICE RawMouse[1];
    RawMouse[0].usUsagePage = HID_USAGE_PAGE_GENERIC;
    RawMouse[0].usUsage = HID_USAGE_GENERIC_MOUSE;
    RawMouse[0].dwFlags = RIDEV_INPUTSINK;
    RawMouse[0].hwndTarget = mMainWindow->GetHandle();

    if (RegisterRawInputDevices(RawMouse, 1, sizeof(RawMouse[0])) == FALSE)
        Logger.Error("Failed to setup raw mouse input", "Core");

    mKeyboard = std::make_unique<cKeyboard>();
    mMouse = std::make_unique<cMouse>();

    // set pointers to keyboard and mouse for the window
    mMainWindow->SetKeyboard(mKeyboard.get());
    mMainWindow->SetMouse(mMouse.get());

    return {RESULT_OK};
}

Result<void> cCore::Init()
{
    return Init(sCoreInitArguments());
}

Result<void> cCore::Init(const sCoreInitArguments& aSettings)
{
    // Logger setup
    Logger.SetSeverityLevel(aSettings.LoggerSeverityLevel);
    Logger.Notify("Starting core initialization..", "Core");
    if (aSettings.LoggerOutput != nullptr)
    {
        auto LoggerResult = Logger.Open(aSettings.LoggerOutput);

        if (Failed(LoggerResult))
        {
            // report
            std::string OutputMessage = "Could not open file '" + std::string(aSettings.LoggerOutput) + std::string("' for logging. Logging output redirected to standard output instead");
            Logger.Error(OutputMessage.c_str(), "Core");
            Logger.Open(std::cout);
        }
    }

    // Open the console as well if it should be visible
    if (aSettings.VisibleConsole)
    {
        BOOL ConsoleSetup = AllocConsole();
        if (ConsoleSetup == 0)	// failed to open new console
            Logger.Error("Failed to create new console window", "Core");
        else
            auto a = freopen("CONOUT$", "w", stdout);	// redirect std out to console window
    }

    // Setup the window
    auto WindowSetup = SetupWindow(aSettings);
    if (Failed(WindowSetup))
    {
        Logger.Error("Failed to create window", "Core");
        return {RESULT_INIT_FAIL};
    }
    else
    {
        Logger.Notify("Window created", "Core");
    }

    // Setup extensions
    auto ExtensionSetup = SetupExtensions();
    if (Failed(ExtensionSetup))
    {
        Logger.Error("Failed to setup some or all extensions", "Core");
    }
    else
    {
        Logger.Notify("Extensions set up correctly", "Core");
    }

    // Setup rendering
    auto RenderingSetup = SetupRendering();
    if (Failed(RenderingSetup))
    {
        Logger.Error("Failed to initialize rendering support", "Core");
        return {RESULT_INIT_FAIL};
    }
    else
    {
        Logger.Notify("Rendering initialized", "Core");
    }

    if (aSettings.Mode == EngineMode::Editor)
    {
        mEditor = std::make_unique<LevelEditor>();
        auto EditorInit = mEditor->Init(this);
        if (Failed(EditorInit)) {
            Logger.Error("Failed to initialize level editor", "Core");
            mEditor = nullptr;
            return {RESULT_INIT_FAIL};
        }
    }
    else if (aSettings.Mode == EngineMode::Game)
    {
        mEditor = std::make_unique<EmptyEditor>();
        mEditor->Init(nullptr);     // init empty editor
    }

    Logger.EnableOpenGlDebugLog();
    ResourceSystem::Instance();		                // initialize resource system for the first time	
    mScene = std::make_unique<cScene>();            // create initial scene
    mIsInitialized = true;							// Core is initialized

    // TEMP: load some map
    // TODO: remove this
    auto& Scene = *mScene;
    auto& Demo = Scene.AddObject();

    glm::mat4 Identity = glm::mat4(1.0f);
    glm::mat4 ScaleDown = glm::scale(Identity, glm::vec3(0.001f));

    constexpr int ModelCount = 23;
    const char* const TextureNames[] = {
        "Assets/Textures/sponza1.png",
        "Assets/Textures/sponza2.png",
        "Assets/Textures/sponza3.png",
        "Assets/Textures/sponza4.png",
        "Assets/Textures/sponza5.png",
        "Assets/Textures/sponza6.png",
        "Assets/Textures/sponza7.png",
        "Assets/Textures/sponza8.png",
        "Assets/Textures/sponza9.png",
        "Assets/Textures/sponza10.png",
        "Assets/Textures/sponza11.png",
        "Assets/Textures/sponza12.png",
        "Assets/Textures/sponza13.png",
        "Assets/Textures/sponza14.png",
        "Assets/Textures/sponza15.png",
        "Assets/Textures/sponza16.png",
        "Assets/Textures/sponza17.png",
        "Assets/Textures/sponza18.png",
        "Assets/Textures/sponza19.png",
        "Assets/Textures/sponza20.png",
        "Assets/Textures/sponza21.png",
        "Assets/Textures/sponza22.png",
        "Assets/Textures/sponza23.png"
    };
    const char* const ModelNames[] = {
        "Assets/Models/sponza1.krmod",
        "Assets/Models/sponza2.krmod",
        "Assets/Models/sponza3.krmod",
        "Assets/Models/sponza4.krmod",
        "Assets/Models/sponza5.krmod",
        "Assets/Models/sponza6.krmod",
        "Assets/Models/sponza7.krmod",
        "Assets/Models/sponza8.krmod",
        "Assets/Models/sponza9.krmod",
        "Assets/Models/sponza10.krmod",
        "Assets/Models/sponza11.krmod",
        "Assets/Models/sponza12.krmod",
        "Assets/Models/sponza13.krmod",
        "Assets/Models/sponza14.krmod",
        "Assets/Models/sponza15.krmod",
        "Assets/Models/sponza16.krmod",
        "Assets/Models/sponza17.krmod",
        "Assets/Models/sponza18.krmod",
        "Assets/Models/sponza19.krmod",
        "Assets/Models/sponza20.krmod",
        "Assets/Models/sponza21.krmod",
        "Assets/Models/sponza22.krmod",
        "Assets/Models/sponza23.krmod"
    };

    const char* ShaderPath = "Assets/Shaders/default.krmat";

    cShaderHandle Material;
    ResourceSystem::Instance().LoadShader(ShaderPath, Material);

    {
        cMeshHandle MeshHandle;
        cTextureHandle TextureHandle;
        ResourceSystem::Instance().LoadMesh(ModelNames[0], MeshHandle);
        ResourceSystem::Instance().LoadTexture(TextureNames[0], TextureHandle);

        Demo.SetMesh(MeshHandle);
        Demo.SetMaterial(Material);
        Demo.SetTexture(TextureHandle);
        Demo.SetTransform(ScaleDown);
    }

    for (int i = 1; i < ModelCount; i++)
    {
        cMeshHandle MeshHandle;
        cTextureHandle TextureHandle;
        ResourceSystem::Instance().LoadMesh(ModelNames[i], MeshHandle);
        ResourceSystem::Instance().LoadTexture(TextureNames[i], TextureHandle);

        cObject& ModelPart = Demo.AddChild();
        ModelPart.SetMesh(MeshHandle);
        ModelPart.SetMaterial(Material);
        ModelPart.SetTexture(TextureHandle);
        ModelPart.SetTransform(Identity);
    }

    return {RESULT_OK};
}

Result<void> cCore::Terminate()
{
    Logger.Notify("Terminating core", "Core");
    mEditor->Terminate();

    return {RESULT_OK};
}

iEditor* cCore::GetEditor()
{
    return mEditor.get();
}

Result<void> cCore::SetupExtensions()
{
    // checks, if it is possible to gain control of swap interval
    if (cCore::WGLExtensionIsSupported("WGL_EXT_swap_control")) {
        // gets us function pointers
        wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");	
        wglGetSwapIntervalEXT = (PFNWGLGETSWAPINTERVALEXTPROC)wglGetProcAddress("wglGetSwapIntervalEXT");
    } else {
        Logger.Warning("Could not gain control of framebuffer swap interval", "Core");
    }

    return {RESULT_OK};
}

bool cCore::WGLExtensionIsSupported(const char* aExtensionName)
{
    PFNWGLGETEXTENSIONSSTRINGEXTPROC _wglGetExtensionsStringEXT = NULL;
    _wglGetExtensionsStringEXT = (PFNWGLGETEXTENSIONSSTRINGEXTPROC)wglGetProcAddress("wglGetExtensionsStringEXT");

    if (strstr(_wglGetExtensionsStringEXT(), aExtensionName) == NULL)		// check if this extension is supported
        return false;

    return true;
}

void cCore::Run()
{
    if (mIsInitialized == false) {
        Logger.Error("Core can not enter main loop because core was not yet initialized", "Core");
        return;
    }

    auto LastFrameTime = std::chrono::system_clock::now();
    auto LastCheckTime = LastFrameTime;
    int FramesPerSecond = 0;
    InputData Input = {};

    while (mMainWindow != nullptr and !mMainWindow->IsClosed())
    {
        //	Get all window messages, this also updates the keyboard and the mouse
        mMainWindow->ProcessMessages();

        // Benchmark
        FramesPerSecond += 1;
        auto TimeNow = std::chrono::system_clock::now();
        auto Delta = TimeNow - LastFrameTime;
        LastFrameTime = TimeNow;

        ProcessInput(Input);
        Update(Delta, Input);
        Render();

        mMouse->Clear();
        mKeyboard->Clear();
    }
}

KR_END