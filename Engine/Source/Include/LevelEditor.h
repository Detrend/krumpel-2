#pragma once

#include <vector>
#include <list>
#include <memory>

#include "Basics.h"
#include "Core.h"
#include "iEditor.h"
#include "Maths.h"

KR_BEGIN

enum class TextureQuality : int
{
    Low = 0,
    Medium = 1, 
    High = 2
};

// Type of tool that is selected in editor
enum class SelectionTool
{
    Move = 0,       // Moves objects
    Rotate = 1,     // Rotates objects
    Scale = 2       // Rescales objects
};

class LevelEditor : public iEditor
{
    private:
        cCore*                            mCore;                    // Pointer to core
        std::list<std::weak_ptr<cObject>> mSelectedObjects;         // List of selected objects
        std::list<std::weak_ptr<cObject>> mInvisibleObjects;        // TODO: implement invisible objects in different way

        // Related to editor settings
        struct {
            bool DevtoolsVisible            = true;             
            bool MainMenuBarEnabled         = true;             // Is main menu bar (top of the screen) visible?
            bool ProfilerVisible            = false;            // Is profiler window visible?
            bool ProfilerGraphicsVisible    = false;            
            bool LoggerSettingsVisible      = false;            // Is the window with logger settings visible?
            float LastFrameTime             = 0.0f;             // time it took to render the last frame
            float AverageFrameTime          = 0.0f;             
            struct {
                bool SettingsChanged        = false;                        // Were graphics settings changed?
                bool ShadowsEnabled         = true;                         // TODO: we do not have shadows
                int ShadowMapResolutionExponent = 8;                        // resoulution = 2^this TODO: does nothing
                TextureQuality TextureResolution = TextureQuality::High;    // TODO: not implemented
            } Graphics;
        } EditorSettings;

        // Everything related to gizmos
        struct {
            // Describes shapes of gizmo lines
            std::vector<glm::vec3> LineX = {{0.0f, 0.0f, 0.0f}, {0.25f, 0.0f, 0.0f}};
            std::vector<glm::vec3> LineY = {{0.0f, 0.0f, 0.0f}, {0.0f, 0.25f, 0.0f}};
            std::vector<glm::vec3> LineZ = {{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.25f}};

            // Describes shapes of circle lines
            std::vector<glm::vec3> CircleX;
            std::vector<glm::vec3> CircleY;
            std::vector<glm::vec3> CircleZ;

            // Colors of gizmo lines
            glm::vec3 Red = {1.0f, 0.0f, 0.0f};
            glm::vec3 Green = {0.0f, 1.0f, 0.0f};
            glm::vec3 Blue = {0.0f, 0.0f, 1.0f};
            glm::vec3 White = {1.0f, 1.0f, 1.0f};

            bool SelectedX = false;         // Is gizmo axis X selected?
            bool SelectedY = false;         // Is gizmo axis Y selected?
            bool SelectedZ = false;         // Is gizmo axis Z selected?

            float SelectDistance = 10.0f;   // The distance from mouse to gizmo to be able to select it (in pixels)
        } Gizmos;

        struct {
            SelectionTool   Edit = SelectionTool::Move;     // Current editing tool
            glm::vec3       MouseInWorldPosPrev = {};       // previous position of mouse projected into world. Used to move objects
            bool            EditEnabled = false;            // If edit mode is enabled. In edit mode you can change position and rotation of objects the in scene
        } Tools;

    private:
        // Returns reference to mouse stored by core
        const cMouse& GetMouse();
        // Returns reference to keyboard stored by core
        const cKeyboard& GetKeyboard();
        // Returns reference to current scene stored by core
        cScene& GetScene();
        // Renders ImGui components (panels, sliders, ...)
        void RenderImGui();
        // Returns pair of camera position and direction of the mouse cursor in the world space
        maths::Ray GetMouseCursorRay();
        // Returns selection plane - the plane with which the mouse ray collides to detect how much selected object should be moved
        // GizmoPosition is the position of selection gizmos in world pos
        // CameraPosition is position of camera in world space
        // SelectedX,Y and Z are bools that determine wheather given gizmo axis is selected by mouse
        static maths::Ray GetSelectionPlane(glm::vec3 GizmoPosition, glm::vec3 CameraPosition, bool SelectedX, bool SelectedY, bool SelectedZ);
        // Returns position of gizmo in world
        // This is the average position of all selected objects
        glm::vec3 GetGizmoPos();
        // Renders Gizmo shape given by array of Points in world space.
        // The transform is transformation applied to all points of the gizmo.
        // If PerspectiveScale is on, then the gizmo will remain the
        // same size on screen no matter the distance of the camera from it.
        // If DepthTest is false then the gizmo will be rendered even when behind objects
        bool Gizmo(glm::mat4 Transform, 
                   glm::vec3 Color,
                   const std::vector<glm::vec3>& Points,
                   int LineWidth,
                   bool PerspectiveScale = true,
                   bool DepthTest = false);
        // Moves all selected objects by certain value
        void MoveAllSelected(glm::vec3 HowMuch);
        // Renders gizmos
        void RenderGizmos();
        // Sets up ImGui library and stuff corelated with it
        Result<> SetupImGui();
        // Renders object in scene node in ImGui tree.
        // This is called for every object in scene.
        // This renders list of all objects in scene from which the objects can be selected
        void ImGuiRenderSceneGraphNode(const std::shared_ptr<cObject>& Object);
        // Terminates ImGui. Called when exiting editor
        Result<> TerminateImGui();

    public:
        LevelEditor();
        virtual Result<> Init(cCore* CoreAddress) override;
        virtual Result<> Terminate() override;
        virtual bool WinProcFunction(HWND, UINT, WPARAM, LPARAM) override;
        virtual void Update() override;
        virtual void Render() override;
        virtual bool BlockMouse() override;
        virtual bool BlockKeyboard() override;
};

KR_END
