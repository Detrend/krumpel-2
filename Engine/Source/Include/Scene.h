#pragma once

#include <list>
#include <memory>

#include "Basics.h"

#include "Camera.h"
#include "Texture.h"
#include "Mesh.h"

//	TODO: Add solid/static objects
//  TODO: Implement 'Update' method

KR_BEGIN

class cScene
{
    private:
        std::list<std::shared_ptr<cObject>>			mObjects;
        std::unique_ptr<cCamera>	                mCamera;
        
    public:
        cScene();
        ~cScene();

        cScene(const cScene&) = delete;
        cScene(cScene&&) = delete;
        cScene& operator=(const cScene&) = delete;

        // Updates the scene
        // TODO: not implemented yet
        void Update(long aMs);
        // Returns const reference to the camera object
        const cCamera& GetCamera() const;
        // Returns reference to the camera object
        cCamera& GetCamera();
        // Adds an empty object to the scene and returns reference to it
        cObject& AddObject();
        // Returns constant list of all objects
        const std::list<std::shared_ptr<cObject>>& GetObjects() const;
        // Returns list of objects in the scene
        std::list<std::shared_ptr<cObject>>& GetObjects();
};

KR_END