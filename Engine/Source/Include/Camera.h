/* Camera object */

#pragma once

#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Basics.h"
#include "Printable.h"
#include "Maths.h"
#include "Object.h"

KR_BEGIN

class cGlRenderer;

class cCamera : public cObject
{
friend class cGlRenderer;
protected:
	glm::mat4 mFrustumMat;
	float mViewFov;
	float mViewAspect;
	float mViewNear;
	float mViewFar;

private:
	//Recalculates projection frustum matrix
	void ResetProjection();

public:
	cCamera();
	cCamera(float FOV, float Aspect, float Near, float Far);
	virtual ~cCamera(){}

	glm::mat4 GetProjection() const { return mFrustumMat; }
	glm::mat4 GetWorldToView() const;
	glm::mat4 GetViewToWorld() const;
	maths::Ray GetRayInWorld(glm::vec3 ScreenSpace);

	void SetProjection(float FOV, float Aspect, float Near, float Far);
	void SetFov(float FOV) { mViewFov = FOV; ResetProjection(); }
	void SetAspect(float Aspect) { mViewAspect = Aspect; ResetProjection(); }
	void SetNearClip(float Distance) { mViewNear = Distance; ResetProjection(); }
	void SetFarClip(float Distance) { mViewFar = Distance; ResetProjection(); }
	void SetFrustumMat(glm::mat4 Frustum) { mFrustumMat = Frustum; }
};

KR_END