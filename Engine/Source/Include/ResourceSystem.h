#pragma once

#include <string>
#include <vector>
#include <memory>
#include <filesystem>
#include <map>

#include "Basics.h"
#include "Shader.h"
#include "Texture.h"
#include "Mesh.h"

KR_BEGIN

// Resource system contains data of all resources we use
//  Contains:
//      Textures
//      Meshes
//      Shaders

//  Access time must be constant and search complexity has to be at least logarithmic


// What this needs to do?
//  Instant access with handles
//  Quick search for a resource
//  Unload old resources from GPU, upload them back once again needed
//  Manage shaders, textures and meshes for us
//  Allow us to not work directly with resources ever again

struct cHandle
{
    int Index = -1;
    int Key = -1;
};

struct cMeshHandle : public cHandle {};

struct cTextureHandle : public cHandle {};

struct cShaderHandle : public cHandle {};

template<typename T>
struct cResourceCell
{
    T Data;
    int Key;
    std::filesystem::path Path;
};

using ShaderCell = cResourceCell<cShader>;
using TextureCell = cResourceCell<cTexture>;
using MeshCell = cResourceCell<cMesh>;

struct CompareByFilePath
{
    bool operator()(const std::filesystem::path& Path1, const std::filesystem::path& Path2) const
    {
        return Path1 < Path2;
    }
};

class ResourceSystem
{
using path = std::filesystem::path;

KR_PRIVATE:
    std::vector<ShaderCell>     mShaderStorage;
    std::vector<TextureCell>    mTextureStorage;
    std::vector<MeshCell>       mMeshStorage;
    std::map<path, cMeshHandle, CompareByFilePath>      mLoadedMeshes;
    std::map<path, cTextureHandle, CompareByFilePath>   mLoadedTextures;
    std::map<path, cShaderHandle, CompareByFilePath>    mLoadedShaders;       

KR_PRIVATE:
    // Checks if mesh from this file is already loaded. If yes, also returns its handle
    bool MeshAlreadyLoaded(const char* FilePath, cMeshHandle& Handle);
    // Checks if texture was already loaded. If yes, also returns its handle
    bool TextureAlreadyLoaded(const char* FilePath, cTextureHandle& Handle);
    // Checks if shader was already loaded in memory. If so, also returns its handle
    bool ShaderAlreadyLoaded(const char* FilePath, cShaderHandle& Handle);

private:
    ResourceSystem();
    ~ResourceSystem();

KR_PUBLIC:
    // Returns instance of this singleton
    static ResourceSystem& Instance();
    // delete copy constructor
    ResourceSystem(const ResourceSystem&) = delete;
    // delete move constructor
    ResourceSystem(ResourceSystem&&) = delete;
    // delete assignment operator
    ResourceSystem& operator=(const ResourceSystem&) = delete;
    // Loads mesh from file path
    eResult LoadMesh(const char* FilePath, cMeshHandle& Handle);
    // Loads texture from file path
    eResult LoadTexture(const char* FilePath, cTextureHandle& Handle);
    // Loads shader from file path
    eResult LoadShader(const char* FilePath, cShaderHandle& Handle);
    // Returns mesh from handle in constant access time
    const cMesh& GetMesh(cMeshHandle Handle);
    // Returns texture from handle in constant access time
    const cTexture& GetTexture(cTextureHandle Handle); 
    // Returns shader from handle in constant access time
    const cShader& GetShader(cShaderHandle Handle);    
};

KR_END