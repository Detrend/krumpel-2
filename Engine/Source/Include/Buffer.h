#pragma once

#include <memory>
#include <utility>
#include <cstring>
#include <cmath>

#include "Basics.h"
#include "Printable.h"
#include "Logger.h"

KR_BEGIN

template<class tType>
class cBuffer
{
protected:
	std::shared_ptr<tType> mData;
	uint mDataSize;

public:
	cBuffer() : mData(), mDataSize(0) {}
	cBuffer(cBuffer<tType>&&);
	~cBuffer() { Free(); }

	inline cBuffer<tType>& operator=(cBuffer<tType>&&);

	inline void Allocate(uint SizeInBytes);
	inline void Allocate(tType* Data, uint SizeInBytes);
	inline void Free();
	inline tType* Get() { return mData.get(); }
	inline uint GetSize() const { return mDataSize; }					//	returns size of data in bytes
	inline uint GetCount() const { return mDataSize / sizeof(tType); }
	inline tType& operator[](uint Index) { return mData.get()[Index]; }
};

template<class tType>
inline void cBuffer<tType>::Allocate(uint aSize)
{
	Free();

	uint tCount = static_cast<uint>(std::ceil(aSize / (double)sizeof(tType)));
	tType* ptr = new(std::nothrow) tType[tCount];

	if (ptr) {
		mData = std::shared_ptr<tType>(ptr, std::default_delete<tType[]>());
		mDataSize = aSize;
	}
}

template<class tType>
cBuffer<tType>::cBuffer(cBuffer<tType>&& aOther)
	: mDataSize(aOther.mDataSize),
	  mData(std::move(aOther.mData))
{
	aOther.mDataSize = 0;
}

template<class tType>
inline cBuffer<tType>& cBuffer<tType>::operator=(cBuffer<tType>&& aOther)
{
	if (this != &aOther) {
		Free();

		mData = std::move(aOther.mData);
		mDataSize = aOther.mDataSize;
		aOther.mDataSize = 0;
	}
	
	return *this;
}

template<class tType>
inline void cBuffer<tType>::Allocate(tType* aData, uint aSize)
{
	Allocate(aSize);
	std::memcpy(mData.get(), aData, aSize);
}

template<class tType>
inline void cBuffer<tType>::Free()
{
	if (mData) {
		mData.reset();
		mDataSize = 0;
	}
}

KR_END