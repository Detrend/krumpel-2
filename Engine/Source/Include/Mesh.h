/* Mesh contains all data required to be rendered to scene and their
   correspondig OpenGL handles.
*/
#pragma once

#include <string>
#include <memory>
#include <glm/glm.hpp>

#include "Basics.h"
#include "Glew.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "Texture.h"

KR_BEGIN

// Every kr model file has to begin with thease 8 symbols. Files are opened in binary mode
constexpr char KR_MODEL_SIGNATURE[8] = { 'k', 'r', ' ', 'm', 'o', 'd', 'e', 'l' };

class cGenerator;

class cMesh : public iNullReference
{
friend class cGenerator;		// Allow generator to generate new meshes
private:
	cVertexBuffer mVertexBufferData;
	cIndexBuffer mIndexBufferData;

	GLuint mVertexArrayHandle;
	GLuint mVertexBufferHandle;
	GLuint mIndexBufferHandle;

	bool mInGpu;				// Is the data available on GPU?

public:
	cMesh();
	cMesh(const cMesh&) = delete;
	// Moves the data from other mesh
	cMesh(cMesh&&) noexcept;
	~cMesh();

	cMesh& operator=(cMesh&&);
	cMesh& operator=(const cMesh&) = delete;

	// Loads model data (vertices and indices) to GPU with OpenGL or DirectX (data remains on RAM)
	void UploadToGpu();
	// Frees data allocated on GPU with OpenGL or DirectX (data remains on RAM)
	void FreeFromGpu();
	// Checks if data is on GPU
	bool InGpu() const;
	// Handle to vertex array
	GLuint GetVertexArrayHandle() const		{ return mVertexArrayHandle; }
	// Handle to vertex buffer
	GLuint GetVertexBufferHandle() const	{ return mVertexBufferHandle; }
	// Handle to index buffer
	GLuint GetIndexBufferHandle() const		{ return mIndexBufferHandle; }
	// Number of indices this mesh has
	GLsizei GetIndexCount() const			{ return mIndexBufferData.GetCount(); }
	// Copies vertices into this mesh from given location. Size is in bytes
	void SetVertexData(float* CopyFrom, uint SizeInBytes);
	// Copies index data into this mesh from given location. Size is in bytes
	void SetIndexData(uint* CopyFrom, uint SizeInBytes);
	// Loads index and vertex data from kr model file of given location
	Result<> LoadFromFile(const char* FilePath);
	// Returns true only for NullMesh. Null mesh is mesh that can not be used
	virtual bool IsNull() const override;
};

// Null mesh global
inline const cMesh NullMesh;

KR_END