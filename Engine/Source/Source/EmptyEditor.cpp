#include "EmptyEditor.h"

KR_BEGIN

Result<> EmptyEditor::Init(cCore* CoreAddress)
{
    return {RESULT_OK};
}

Result<> EmptyEditor::Terminate()
{
    return {RESULT_OK};
}

void EmptyEditor::Update()
{
}

void EmptyEditor::Render()
{
}

bool EmptyEditor::WinProcFunction(HWND, UINT, WPARAM, LPARAM)
{
    return false;
}

bool EmptyEditor::BlockMouse()
{
    return false;
}

bool EmptyEditor::BlockKeyboard()
{
    return false;
}

KR_END
