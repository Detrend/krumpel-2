/* Object visible in the scene */

#pragma once

#include <string>
#include <memory>
#include <list>

#include "Basics.h"
#include "Mesh.h"
#include "Printable.h"
#include "ResourceSystem.h"

KR_BEGIN

inline int ObjectCounter = 0;		// Helps us determine an unique ID for each new object. Incremented with creation of every object

class cObject
{
private:
    std::list<std::shared_ptr<cObject>>	mChildren;
    cMeshHandle			mMesh;
    cTextureHandle		mTexture;
    cShaderHandle		mMaterial;
    int					mObjectId;
    bool				mIsSelected;
    glm::mat4			mObjectTransform;

public:
    cObject();
    cObject(const cObject&) = delete;
    ~cObject();

    // Sets a new worls space position for the object
    void SetPosition(glm::vec3 NewPosition);
    // Returns its position
    glm::vec3 GetPosition() const;
    // Sets new scale (x, y, z) for the object
    void SetScale(glm::vec3 Scale);
    // Returns current scale
    glm::vec3 GetScale() const;
    // Applies matrix transformation to the object
    void Transform(const glm::mat4& TransformationMatrix);
    // Sets new transformation. This replaces the old one. This changes position, scale and rotation
    void SetTransform(const glm::mat4& TransformationMatrix);
    // Returns current transformation matrix
    const glm::mat4& GetTransform() const;
    // Rotates the object around X axis in degrees (ccw)
    void RotateX(float EulerDegrees);
    // Rotates the object around Y axis in degrees (ccw)
    void RotateY(float EulerDegrees);
    // Rotates the object around Z axis in degrees (ccw)
    void RotateZ(float EulerDegrees);
    // Returns vector of current euler rotation angles. Component X is rotation around X and so on..
    glm::vec3 GetRotationAngles() const;
    // Returns rotation matrix of this object
    glm::mat3 GetRotationMatrix() const;
    // Sets new rotation (x, y, z) of this object
    void SetRotationAngles(glm::vec3 RotationXYZ);
    // Sets new rotation from given matrix
    void SetRotationMatrix(const glm::mat3& Rotation);
    // Adds a child to the object
    cObject& AddChild();
    // Returns constant list of objects children
    const std::list<std::shared_ptr<cObject>>& GetChildren() const;
    // Returns list of objects children
    std::list<std::shared_ptr<cObject>>& GetChildren();
    // Returns ID of this exact object
    int GetId() const;
    // Returns mesh handle of this object
    cMeshHandle GetMesh() const;
    // Sets new mesh for this object
    void SetMesh(cMeshHandle Handle);
    // Returns shader/material handle of this object
    cShaderHandle GetMaterial() const;
    // Assigns a new shader/material to this object
    void SetMaterial(cShaderHandle Handle);
    // Returns a texture handle of this object
    cTextureHandle GetTexture() const;
    // Sets a new texture for this object
    void SetTexture(cTextureHandle Handle);
    // Checks wheather the object is currently selected
    bool IsSelected() const;
    // Set selection state. True = selected
    void SetSelectedState(bool State);
};

KR_END