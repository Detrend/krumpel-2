#version 330 core
#extension GL_ARB_explicit_uniform_location : require   

in vec2 fTexPos;
out vec4 FragColor;

layout (location = 3) uniform sampler2D Texture;

void main()
{
	FragColor = vec4(texture(Texture, fTexPos).rgb, 1.0f);
}