#include <iostream>
#include <windowsx.h>
#include <Windows.h>

#include "Mouse.h"
#include "Window.h"

KR_BEGIN

bool cMouse::IsPressed(MouseButton aButton) const
{
	return mPressedButtons[(std::size_t)aButton];
}

bool cMouse::IsChecked(MouseButton aButton) const
{
	return mCheckedButtons[(std::size_t)aButton];
}

bool cMouse::IsReleased(MouseButton aButton) const
{
	return mReleasedButtons[(std::size_t)aButton];
}

void cMouse::SetChecked(MouseButton aButton, bool aState)
{
	mCheckedButtons[(std::size_t)aButton] = aState;
}

void cMouse::SetPressed(MouseButton aButton, bool aState)
{
	mPressedButtons[(std::size_t)aButton] = aState;
}

void cMouse::SetReleased(MouseButton aButton, bool aState)
{
	mReleasedButtons[(std::size_t)aButton] = aState;
}

void cMouse::SetCoord(int64 aXcoord, int64 aYcoord)
{
	mXCoord = aXcoord;
	mYCoord = aYcoord;
}

void cMouse::SetChange(int64 aXCoord, int64 aYCoord)
{
	mXChange = aXCoord;
	mYChange = aYCoord;
}

int64 cMouse::GetMouseX() const
{
	return mXCoord;
}

int64 cMouse::GetMouseY() const
{
	return mYCoord;
}

int64 cMouse::GetChangeX() const
{
	return mXChange;
}

int64 cMouse::GetChangeY() const
{
	return mYChange;
}

void cMouse::SetCursorPosition(const cWindow& aWindow, int64 aX, int64 aY)
{
	HWND Handle = aWindow.GetHandle();
	POINT p = {static_cast<LONG>(aX), static_cast<LONG>(aY)};
	if (Handle != NULL)
        ClientToScreen(Handle, &p);
	SetCursorPos(p.x, p.y);
}

void cMouse::Clear()
{
	mXChange = 0;
	mYChange = 0;
	mPressedButtons.reset();
	mReleasedButtons.reset();
}

void HandleMouseEvent(HWND aWindow, UINT aMsg, cMouse* aMouse, WPARAM aWparam, LPARAM aLparam)
{
	if (aMouse != nullptr)
	{
		switch (aMsg)
		{
		// Raw mouse device input
		case WM_INPUT:
			{
                unsigned size = sizeof(RAWINPUT);
                static RAWINPUT raw[sizeof(RAWINPUT)];
                GetRawInputData((HRAWINPUT)aLparam, RID_INPUT, raw, &size, sizeof(RAWINPUTHEADER));

                if (raw->header.dwType == RIM_TYPEMOUSE)
				{
					// MOUSE_MOVE_RELATIVE = relative mouse movement both inside and outside of the window
					// MOUSE_MOVE_ABSOLUTE = relative mouse movement only inside of the window
					if ((raw->data.mouse.usFlags & MOUSE_MOVE_RELATIVE) == MOUSE_MOVE_RELATIVE)
					{
                        int64 xc = raw->data.mouse.lLastX;
                        int64 yc = raw->data.mouse.lLastY;
                        if (aMouse != nullptr)
                        {
                            aMouse->SetChange(xc, yc);
                        }
					}

					// TODO: implement this later
                    //if (raw->data.mouse.usButtonFlags & RI_MOUSE_WHEEL)
                    //input.mouse.wheel = (*(short*)&raw->data.mouse.usButtonData) / WHEEL_DELTA;
                }
			}
			break;
		case WM_MOUSEMOVE:
			aMouse->SetCoord(GET_X_LPARAM(aLparam), GET_Y_LPARAM(aLparam));
            break;
		case WM_LBUTTONDOWN:
			if (!aMouse->IsChecked(MouseButton::Left)) {
				aMouse->SetChecked(MouseButton::Left, true);
				aMouse->SetPressed(MouseButton::Left, true);
			}
			break;
		case WM_LBUTTONUP:
			if (aMouse->IsChecked(MouseButton::Left)) {
				aMouse->SetChecked(MouseButton::Left, false);
				aMouse->SetPressed(MouseButton::Left, false);
				aMouse->SetReleased(MouseButton::Left, true);
			}
			break;
		case WM_RBUTTONDOWN:
			if (!aMouse->IsChecked(MouseButton::Right)) {
				aMouse->SetChecked(MouseButton::Right, true);
				aMouse->SetPressed(MouseButton::Right, true);
			}
			break;
		case WM_RBUTTONUP:
			if (aMouse->IsChecked(MouseButton::Right)) {
				aMouse->SetChecked(MouseButton::Right, false);
				aMouse->SetPressed(MouseButton::Right, false);
				aMouse->SetReleased(MouseButton::Right, true);
			}
			break;
		case WM_MBUTTONDOWN:
			if (!aMouse->IsChecked(MouseButton::Middle)) {
				aMouse->SetChecked(MouseButton::Middle, true);
				aMouse->SetPressed(MouseButton::Middle, true);
			}
			break;
		case WM_MBUTTONUP:
			if (aMouse->IsChecked(MouseButton::Middle)) {
				aMouse->SetChecked(MouseButton::Middle, false);
				aMouse->SetPressed(MouseButton::Middle, false);
				aMouse->SetReleased(MouseButton::Middle, true);
			}
			break;
		case WM_XBUTTONDOWN:
			break;
		case WM_XBUTTONUP:
			break;
		}
	}
	else
	{
		KR_BREAKPOINT;
	}
}

KR_END