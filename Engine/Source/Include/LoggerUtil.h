#pragma once

#include "Printable.h"
#include "Basics.h"
#include <string>

KR_BEGIN

struct cPrintableCharacter : public cPrintable
{
private:
    const char* mCharacters;

public:
    cPrintableCharacter(const char* Characters) : mCharacters(Characters) {};

    std::string Represent() const override;
};

struct cPrintableColor : public cPrintable
{
private:
    int mColorId;
public:
    cPrintableColor(int ColorId) : mColorId(ColorId) {};

    std::string Represent() const override;
};

KR_END