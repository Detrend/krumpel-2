#pragma once

#include "Basics.h"

#include "iEditor.h"

KR_BEGIN

class EmptyEditor : public iEditor
{
    public:
        virtual Result<> Init(cCore* CoreAddress) override;

        virtual Result<> Terminate() override;

        virtual void Update() override;

        virtual void Render() override;

        virtual bool WinProcFunction(HWND, UINT, WPARAM, LPARAM) override;

        virtual bool BlockMouse() override;

        virtual bool BlockKeyboard() override;
};

KR_END
