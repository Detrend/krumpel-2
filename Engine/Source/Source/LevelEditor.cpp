#include "LevelEditor.h"
#include "Core.h"
#include "Window.h"
#include "Maths.h"

#include "Imgui.h"

#include "glm/gtc/matrix_transform.hpp"

extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

KR_BEGIN

Result<> LevelEditor::SetupImGui()
{
    constexpr const char* glsl_version = "#version 130";

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();

    // Setup Platform/Renderer backends
    auto r1 = ImGui_ImplWin32_Init(mCore->mMainWindow->GetHandle());
    auto r2 = ImGui_ImplOpenGL3_Init(glsl_version);

    if (r1 != true or r2 != true) {
        return {RESULT_INIT_FAIL};
    } else {
        return {RESULT_OK};
    }
}

void LevelEditor::ImGuiRenderSceneGraphNode(const std::shared_ptr<cObject>& aObject)
{
    if (mCore->mScene == nullptr or aObject == nullptr)
        return;

    auto&& SelectedActors = mSelectedObjects;
    auto&& Children = aObject->GetChildren();
    ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_FramePadding | ImGuiTreeNodeFlags_SpanAvailWidth;

    // Check if this actor is selected
    bool IsSelected = std::find_if(SelectedActors.begin(), SelectedActors.end(), [&](const std::weak_ptr<cObject>& Val){
        return Val.lock().get() == aObject.get();
    }) != SelectedActors.end();

    if (Children.empty())
        flags |= ImGuiTreeNodeFlags_Leaf;
    if (IsSelected)
        flags |= ImGuiTreeNodeFlags_Framed;

    // Get id
    std::string StrId = std::to_string(aObject->GetId());

    // Draw
    if (ImGui::TreeNodeEx(StrId.c_str(), flags)) {
        if (ImGui::IsItemClicked(ImGuiMouseButton_Left) and !ImGui::IsItemToggledOpen()) {
            if (!IsSelected) {       // Select the object
                mSelectedObjects.push_back(std::weak_ptr(aObject));
                aObject->SetSelectedState(true);
            } else {                // Deselect the object
                mSelectedObjects.remove_if([&](const std::weak_ptr<cObject>& Val){
                    return Val.lock().get() == aObject.get();
                });
                aObject->SetSelectedState(false);
            }
        }
        // Recursively call for all children of the object
        for (auto&& Child : Children) {
            ImGuiRenderSceneGraphNode(Child);
        }
        ImGui::TreePop();
    }
}

Result<> LevelEditor::TerminateImGui()
{
    // terminate ImGui
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplWin32_Shutdown();
    ImGui::DestroyContext();

    return {RESULT_OK};
}

const cMouse& LevelEditor::GetMouse()
{
    return *mCore->mMouse;
}

const cKeyboard& LevelEditor::GetKeyboard()
{
    return *mCore->mKeyboard;
}

cScene& LevelEditor::GetScene()
{
    return *mCore->mScene;
}

void LevelEditor::RenderImGui()
{
    if (!EditorSettings.DevtoolsVisible)
        return;

    // Start ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();

    float MenuBarHeight = 0.0f;

    // Main menu bar
    if (EditorSettings.MainMenuBarEnabled && ImGui::BeginMainMenuBar()) {
        if (ImGui::BeginMenu("File")) {
            ImGui::Text("Level name");
            ImGui::Separator();
            ImGui::MenuItem("Save", "Ctrl+S");
            ImGui::MenuItem("Save As", "Ctrl+Shift+S");
            ImGui::MenuItem("Open", "Ctrl+O");
            ImGui::MenuItem("Export", "Ctrl+E");
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Edit")) {
            ImGui::MenuItem("Undo", "Ctrl+Z", nullptr, false);
            ImGui::MenuItem("Redo", "Ctrl+Y", nullptr, false);
            ImGui::Separator();
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Tools")) {
            ImGui::MenuItem("Performance");
            ImGui::Separator();
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Settings")) {
            // Profiler window popup
            if (ImGui::MenuItem("Profiler")) {
                EditorSettings.ProfilerVisible = true;
            }

            // 
            ImGui::MenuItem("Editor");

            // Logger settings window popup
            if (ImGui::MenuItem("Logging")) {
                EditorSettings.LoggerSettingsVisible = true;
            }
            ImGui::EndMenu();
        }

        MenuBarHeight = ImGui::GetWindowHeight();

        ImGui::EndMainMenuBar();
    }

    // Profiler window
    if (EditorSettings.ProfilerVisible) {
        ImGui::Begin("Profiler", &EditorSettings.ProfilerVisible);

        float AFps = 1000.0f / EditorSettings.AverageFrameTime;
        ImVec4 CounterColor = AFps < 30.0f ? ImVec4(1.0f, 0.0f, 0.0f, 1.0f) : (AFps < 60.0f ? ImVec4(1.0f, 1.0f, 0.0f, 1.0f) : ImVec4(0.0f, 1.0f, 0.0f, 1.0f));

        ImGui::Text("Frametime: ", EditorSettings.AverageFrameTime); ImGui::SameLine();
        ImGui::TextColored(CounterColor, "%.3fms", EditorSettings.AverageFrameTime);
        ImGui::Text("FPS: "); ImGui::SameLine();
        ImGui::TextColored(CounterColor, "%.3f", AFps);

        constexpr const char* TextureQualityNames[] = {"Low", "Medium", "High"};

        ImGui::Separator();
        if (ImGui::TreeNode("Graphic settings")) {
            // Apply button
            ImGui::BeginDisabled(!EditorSettings.Graphics.SettingsChanged);
            if (ImGui::Button("Apply")) {
                // Apply graphic settings
                EditorSettings.Graphics.SettingsChanged = false;
            }
            ImGui::EndDisabled();

            // Enable/disable shadows
            if (ImGui::Checkbox("Shadows", &EditorSettings.Graphics.ShadowsEnabled))
                EditorSettings.Graphics.SettingsChanged = true;

            // Shadow map resolution settings
            ImGui::BeginDisabled(!EditorSettings.Graphics.ShadowsEnabled);
            if (ImGui::SliderInt("Shadow map resolution", &EditorSettings.Graphics.ShadowMapResolutionExponent, 6, 12, 
            "%d", ImGuiSliderFlags_AlwaysClamp))
                EditorSettings.Graphics.SettingsChanged = true;
            ImGui::EndDisabled();

            // Texture resolution slider
            if (ImGui::SliderInt("Texture resolution", (int*)&EditorSettings.Graphics.TextureResolution, 0, 2, 
            TextureQualityNames[(int)EditorSettings.Graphics.TextureResolution], ImGuiSliderFlags_AlwaysClamp)) 
                EditorSettings.Graphics.SettingsChanged = true;

            ImGui::TreePop();
        }
        
        ImGui::End();
    }

    // Logger settings window
    if (EditorSettings.LoggerSettingsVisible) {
        ImGui::Begin("Logging settings", &EditorSettings.LoggerSettingsVisible);
        ImGui::Text("Logging severity");

        // Engine section
        bool LogNothing = Logger.GetSeverityLevel() == LoggerSeverityLevel::ReportNothing ? true : false;
        bool LogErrors = Logger.GetSeverityLevel() == LoggerSeverityLevel::ReportOnlyCritical ? true : false;
        bool LogWarnings = Logger.GetSeverityLevel() == LoggerSeverityLevel::ReportWarnings ? true : false;
        bool LogEverything = Logger.GetSeverityLevel() == LoggerSeverityLevel::ReportEverything ? true : false;

        ImGui::Text("What should engine report:");
        if (ImGui::Checkbox("Nothing", &LogNothing) and LogNothing == true) {
            LogErrors = false; LogWarnings = false; LogEverything = false;
            Logger.SetSeverityLevel(LoggerSeverityLevel::ReportNothing);
        }
        if (ImGui::Checkbox("Only errors", &LogErrors) and LogErrors == true) {
            LogNothing = false; LogWarnings = false; LogEverything = false;
            Logger.SetSeverityLevel(LoggerSeverityLevel::ReportOnlyCritical);
        }
        if (ImGui::Checkbox("Warnings and errors", &LogWarnings) and LogWarnings == true) {
            LogErrors = false; LogNothing = false; LogEverything = false;
            Logger.SetSeverityLevel(LoggerSeverityLevel::ReportWarnings);
        }
        if (ImGui::Checkbox("Everything", &LogEverything) and LogEverything == true) {
            LogErrors = false; LogWarnings = false; LogNothing = false;
            Logger.SetSeverityLevel(LoggerSeverityLevel::ReportEverything);
        }
        ImGui::Separator();

        // OpenGl section
        LogNothing = Logger.GetOpenGlSeverityLevel() == LoggerSeverityLevel::ReportNothing ? true : false;
        LogErrors = Logger.GetOpenGlSeverityLevel() == LoggerSeverityLevel::ReportOnlyCritical ? true : false;
        LogWarnings = Logger.GetOpenGlSeverityLevel() == LoggerSeverityLevel::ReportWarnings ? true : false;
        LogEverything = Logger.GetOpenGlSeverityLevel() == LoggerSeverityLevel::ReportEverything ? true : false;

        ImGui::Text("What should OpenGl report:");
        if (ImGui::Checkbox("Nothing ", &LogNothing) and LogNothing == true) {
            LogErrors = false; LogWarnings = false; LogEverything = false;
            Logger.SetOpenGlSeverityLevel(LoggerSeverityLevel::ReportNothing);
        }
        if (ImGui::Checkbox("Only errors ", &LogErrors) and LogErrors == true) {
            LogNothing = false; LogWarnings = false; LogEverything = false;
            Logger.SetOpenGlSeverityLevel(LoggerSeverityLevel::ReportOnlyCritical);
        }
        if (ImGui::Checkbox("Warnings and errors ", &LogWarnings) and LogWarnings == true) {
            LogErrors = false; LogNothing = false; LogEverything = false;
            Logger.SetOpenGlSeverityLevel(LoggerSeverityLevel::ReportWarnings);
        }
        if (ImGui::Checkbox("Everything ", &LogEverything) and LogEverything == true) {
            LogErrors = false; LogWarnings = false; LogNothing = false;
            Logger.SetOpenGlSeverityLevel(LoggerSeverityLevel::ReportEverything);
        }
        ImGui::Separator();

        ImGui::End();
    }

    // Object list
    ImGui::SetNextWindowPos(ImVec2(0, MenuBarHeight));
    ImGui::SetNextWindowSizeConstraints(ImVec2(100, mCore->mMainWindow->GetHeight() - MenuBarHeight), 
                                        ImVec2(mCore->mMainWindow->GetWidth() / 2.0f, mCore->mMainWindow->GetHeight() - MenuBarHeight));

    ImGui::Begin("[Scene name]", nullptr, ImGuiWindowFlags_NoMove);
    if (mCore->mScene != nullptr) {
        for (auto&& Child : mCore->mScene->GetObjects()) {
            if (Child != nullptr) {
                ImGuiRenderSceneGraphNode(Child);
            }
        }
    }
    ImGui::End();

    // Object info
    ImGui::SetNextWindowPos(ImVec2(static_cast<float>(mCore->mMainWindow->GetWidth()), MenuBarHeight), 0, ImVec2(1.0f, 0.0f));
    ImGui::SetNextWindowSizeConstraints(ImVec2(100, mCore->mMainWindow->GetHeight() - MenuBarHeight), 
                                        ImVec2(mCore->mMainWindow->GetWidth() / 2.0f, mCore->mMainWindow->GetHeight() - MenuBarHeight));
    ImGui::Begin("Object properties", nullptr, ImGuiWindowFlags_NoMove);
    ImGui::Text("Random text");
    ImGui::End();

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

bool LevelEditor::Gizmo(glm::mat4 aTransform,
                        glm::vec3 aColor,
                        const std::vector<glm::vec3>& aPoints,
                        int aLineWidth,
                        bool aPerspective,
                        bool aDepthTest)
{
    if (mCore->mScene == nullptr)
        return false;

    bool NearLine = false;      // does mouse hover over the line?

    const glm::mat4& Frustum = mCore->mScene->GetCamera().GetProjection();
    const glm::mat4& WorldToCamera = mCore->mScene->GetCamera().GetWorldToView();

    mCore->mRenderer3D->RenderLines(mCore->mScene->GetCamera(), aTransform, aColor, aPoints, aDepthTest, aLineWidth, aPerspective);

    if (aPerspective) {
        glm::vec4 Zero = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
        glm::vec3 Location = glm::vec3(aTransform * Zero);
        glm::vec3 Up = glm::vec3(0.0f, 1.0f, 0.0f);
        glm::mat4 LookAtDirectly = glm::lookAt(mCore->mScene->GetCamera().GetPosition(), Location, Up);
        glm::vec4 ScreenPosition = Frustum * LookAtDirectly * aTransform * Zero;
        float ScaleFactor = ScreenPosition.w;
        aTransform = aTransform * glm::scale(glm::mat4(1.0f), glm::vec3(ScaleFactor));
    }

    // check every pair of points whether mouse hovers over them
    for (std::size_t i = 0; i < aPoints.size(); i += 2) {
        glm::vec4 Point1 = Frustum * WorldToCamera * aTransform * glm::vec4(aPoints[i], 1.0f);
        Point1 /= Point1.w;
        glm::vec4 Point2 = Frustum * WorldToCamera * aTransform * glm::vec4(aPoints[i+1], 1.0f);
        Point2 /= Point2.w;

        glm::vec2 WindowSize = glm::vec2(mCore->mMainWindow->GetWidth(), mCore->mMainWindow->GetHeight());
        glm::vec2 Point1OnScreen = ((glm::vec2(Point1.x, -Point1.y) + glm::vec2(1.0f)) * 0.5f) * WindowSize;       // in screen space
        glm::vec2 Point2OnScreen = ((glm::vec2(Point2.x, -Point2.y) + glm::vec2(1.0f)) * 0.5f) * WindowSize;

        glm::vec2 Mouse = glm::vec2(static_cast<float>(mCore->mMouse->GetMouseX()), static_cast<float>(mCore->mMouse->GetMouseY()));      // mouse coords

        float distanceToLine = maths::DistanceFromFiniteLine2D(Point1OnScreen, Point2OnScreen, Mouse);

        if (distanceToLine <= Gizmos.SelectDistance) {
            NearLine = true;
            break;
        }
    }

    return NearLine;
}

void LevelEditor::MoveAllSelected(glm::vec3 aHowMuch)
{
    for (auto& Selected : mSelectedObjects) {
        if (auto Ptr = Selected.lock(); Ptr) {
            Ptr->SetPosition(Ptr->GetPosition() + aHowMuch);
        }
    }
}

void LevelEditor::RenderGizmos()
{
    if (mCore->mScene == nullptr)
        return;

    if (mSelectedObjects.size() > 0)
    {
        glm::vec3 AveragePos = GetGizmoPos();
        glm::mat4 GizmoTranslation = glm::translate(glm::mat4(1.0f), AveragePos);

        glm::vec3 ColorX = Gizmos.SelectedX ? Gizmos.White : Gizmos.Red;
        glm::vec3 ColorY = Gizmos.SelectedY ? Gizmos.White : Gizmos.Green;
        glm::vec3 ColorZ = Gizmos.SelectedZ ? Gizmos.White : Gizmos.Blue;

        // Render gizmos and check wheather they intersect with mouse
        auto GX = Gizmo(GizmoTranslation, ColorX, Gizmos.LineX, 2);
        auto GY = Gizmo(GizmoTranslation, ColorY, Gizmos.LineY, 2);
        auto GZ = Gizmo(GizmoTranslation, ColorZ, Gizmos.LineZ, 2);

        // If the editing of the selected object is in progress then do not change selection axis
        if (Tools.EditEnabled == false) {
            if (GX) {
                Gizmos.SelectedX = true; Gizmos.SelectedY = false; Gizmos.SelectedZ = false;
            } else if (GY) {
                Gizmos.SelectedX = false; Gizmos.SelectedY = true; Gizmos.SelectedZ = false;
            } else if (GZ) {
                Gizmos.SelectedX = false; Gizmos.SelectedY = false; Gizmos.SelectedZ = true;
            } else {
                Gizmos.SelectedX = false; Gizmos.SelectedY = false; Gizmos.SelectedZ = false;
            }
        }
    }
}

LevelEditor::LevelEditor() : mCore(nullptr)
{
    
}

Result<> LevelEditor::Init(cCore* aCoreAddress)
{
    mCore = aCoreAddress;

    // Init ImGui
    auto ImGuiSetup = SetupImGui();
    if (Failed(ImGuiSetup))
    {
        Logger.Error("Failed to init ImGui", "Level Editor");
        return {RESULT_INIT_FAIL};
    }
    else
    {
        return {RESULT_OK};
    }
}

Result<> LevelEditor::Terminate()
{
    auto ImGuiTermination = TerminateImGui();
    if (Failed(ImGuiTermination))
        return {RESULT_FAIL};

    return {RESULT_OK};
}

bool LevelEditor::WinProcFunction(HWND aH, UINT aU, WPARAM aW, LPARAM aL)
{
    return ::ImGui_ImplWin32_WndProcHandler(aH, aU, aW, aL);
}

maths::Ray LevelEditor::GetMouseCursorRay()
{
    glm::vec3 MouseScreenCoords = {
        (mCore->mMouse->GetMouseX() / (float)mCore->mMainWindow->GetWidth()) * 2.0f - 1.0f,
        ((mCore->mMouse->GetMouseY() / (float)mCore->mMainWindow->GetHeight()) * 2.0f - 1.0f) * -1.0f,
        0.0f
    };
    auto Ray = mCore->mScene->GetCamera().GetRayInWorld(MouseScreenCoords);
    return maths::Ray{Ray.origin, glm::normalize(Ray.direction)};
}

maths::Ray LevelEditor::GetSelectionPlane(glm::vec3 aGizmoPos, glm::vec3 aCameraPos, bool aSelectedX, bool aSelectedY, bool aSelectedZ)
{
    switch (aSelectedX + aSelectedY + aSelectedZ)
    {
    case 0:
        return maths::Ray{};
    case 1: {
            glm::vec3 P1 = aGizmoPos;
            glm::vec3 DirectionToCamera = aCameraPos - P1;
            if (aSelectedX)
                DirectionToCamera.x = 0.0f;
            else if (aSelectedY)
                DirectionToCamera.y = 0.0f;
            else
                DirectionToCamera.z = 0.0f;
            return maths::Ray{P1, glm::normalize(DirectionToCamera)};
        }
    default:
        throw std::runtime_error(std::string("Not implemented"));
    }
}

glm::vec3 LevelEditor::GetGizmoPos()
{
    glm::vec3 AveragePos(0.0f);
    int Count = 0;
    for (auto&& Selected : mSelectedObjects) {
        if (auto Shared = Selected.lock(); Shared) {
            AveragePos += Shared->GetPosition();
            Count++;
        }
    }
    AveragePos /= Count;
    return AveragePos;
}

void LevelEditor::Update()
{
    if (Tools.Edit == SelectionTool::Move)
    {
        // We use this to calculate how much the object has moved when editing it
        maths::Ray SelectionPlane = GetSelectionPlane(GetGizmoPos(),
                                                      GetScene().GetCamera().GetPosition(),
                                                      Gizmos.SelectedX,
                                                      Gizmos.SelectedY,
                                                      Gizmos.SelectedZ);
        if (Tools.EditEnabled)
        {
            if (GetMouse().IsReleased(MouseButton::Left)) {
                // If mouse was released then disable edit mode
                Tools.EditEnabled = false;
            } else {
                // Move all selected objects by the amount how much mouse moved from previous frame
                if (glm::vec3 MousePosNow; maths::RayPlaneIntersects(GetMouseCursorRay(), SelectionPlane, MousePosNow)) {
                    glm::vec3 Difference = MousePosNow - Tools.MouseInWorldPosPrev;                 // difference in world pos
                    Tools.MouseInWorldPosPrev = MousePosNow;
                    Difference *= glm::vec3{Gizmos.SelectedX, Gizmos.SelectedY, Gizmos.SelectedZ};  // restrict the movement only to selected axis
                    MoveAllSelected(Difference);                                                    // move all selected objects
                }
            }
        }
        else
        {
            // If at least one axis is selected and the mouse is pressed then
            // save projection of mouse into the world and enable edit mode
            if ((Gizmos.SelectedX or Gizmos.SelectedY or Gizmos.SelectedZ) && GetMouse().IsPressed(MouseButton::Left)) {
                if (glm::vec3 Result; maths::RayPlaneIntersects(GetMouseCursorRay(), SelectionPlane, Result)) {
                    Tools.EditEnabled = true;
                    Tools.MouseInWorldPosPrev = Result;
                }
            }
        }
    }
}

void LevelEditor::Render()
{
    RenderGizmos();
    RenderImGui();
}

bool LevelEditor::BlockMouse()
{
    return false;
}

bool LevelEditor::BlockKeyboard()
{
    return false;
}

KR_END
