**Cleanup**

move all headers to one folder
move all implementation to one folder

**Overhaul**

For rendering one object we need:
	ibo
	vbo
	texture


**Solid**
long array - size does not change during runtime
quick iterations
each array entry contains ids for ibo, vbo & texture
no pointers to other regions of memory, data must be close together
implement with std::vector

**Entities**
tree - quick access, remove and add
quick iterations
implement with std::map