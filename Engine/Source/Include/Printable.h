#pragma once

#include "Basics.h"
#include <string>

KR_BEGIN

/*	Abstract class for printing state of objects. Every derived object that overrides 
	Represent function can be printed by logger. */
struct cPrintable
{
	/* Override this function in derived class */
	virtual std::string Represent() const = 0;
};

KR_END