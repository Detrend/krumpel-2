#include "Maths.h"

KR_BEGIN

float maths::DistanceFromFiniteLine2D(glm::vec2 aLinePoint1, glm::vec2 aLinePoint2, glm::vec2 aPoint)
{
    aLinePoint2 -= aLinePoint1;
    aPoint -= aLinePoint1;
    aLinePoint1 = glm::vec2(0.0f);

    glm::vec2 ProjectionOnLine = glm::dot(aPoint, glm::normalize(aLinePoint2)) * glm::normalize(aLinePoint2);
    float DistanceFromP1 = glm::distance(aLinePoint1, ProjectionOnLine);        // distance of projected point
    float DistanceFromP2 = glm::distance(aLinePoint2, ProjectionOnLine);
    float LineLength = glm::length(aLinePoint2);

    if (DistanceFromP1 >= LineLength and DistanceFromP2 >= LineLength) {
        if (DistanceFromP1 < DistanceFromP2) {
            ProjectionOnLine = aLinePoint1;
        } else {
            ProjectionOnLine = aLinePoint2;
        }
    } else if (DistanceFromP1 >= LineLength) {
        ProjectionOnLine = aLinePoint2;             // snaps to p2
    } else if (DistanceFromP2 >= LineLength) {
        ProjectionOnLine = aLinePoint1;             // snaps to p1
    }

    float DistanceFromLine = glm::distance(ProjectionOnLine, aPoint);

    return DistanceFromLine;
}

bool maths::RayPlaneIntersects(Ray aRay, Ray aPlane, glm::vec3& aIntersectPoint)
{
    aRay.direction = glm::normalize(aRay.direction);
    aPlane.direction = glm::normalize(aPlane.direction);
    float denom = glm::dot(aPlane.direction, aRay.direction);
    if (std::abs(denom) > 0.0001f) {
        float t = glm::dot(aPlane.origin - aRay.origin, aPlane.direction) / denom;
        if (t >= 0) {
            aIntersectPoint = aRay.origin + aRay.direction * t;
            return true;
        }
    }
    return false;
}

KR_END
