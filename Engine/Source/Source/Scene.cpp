#include "Scene.h"

#include "Object.h"
#include "Camera.h"

KR_BEGIN

cScene::cScene() : mCamera(std::make_unique<cCamera>(100.0f, 16.0f / 9.0f, 0.01f, 10000.0f))
{
    Logger.Notify("Scene constructed", "Scene");
}

cScene::~cScene()
{
}

void cScene::Update(long aMs)
{
    // do nothing
}

const cCamera& cScene::GetCamera() const
{
    return *mCamera.get();
}

cCamera& cScene::GetCamera()
{
    return *mCamera.get();
}

cObject& cScene::AddObject()
{
    std::shared_ptr<cObject>& ObjectPtr = mObjects.emplace_back(std::make_shared<cObject>());   
    return *ObjectPtr.get();
}

const std::list<std::shared_ptr<cObject>>& cScene::GetObjects() const
{
    return mObjects;
}

std::list<std::shared_ptr<cObject>>& cScene::GetObjects()
{
    return mObjects;
}

KR_END