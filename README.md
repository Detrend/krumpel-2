# Krumpel Game Engine

The Krumpel Game Engine was created as material to practice and improve my C++ and OpenGL skills on.

![alt text](Thumbnails/thumb1.png)

The final goal of this project is to create small game in this engine and release it.

![alt text](Thumbnails/thumb2.png)

**Finished Features: (Main branch)**

✅Basic logger

✅Window creation and handling using WinApi

✅Simple resource manager

✅Texture loading

✅Improvised model loading

✅Rendering a simple scene

✅Basic level editor (using ImGui)

**Upcoming Features: (Features branch)**

🟩Load, save and export levels

🟩More advanced level editor

🟩Support more types of files for model loading

**Will be done later in developement:**

🟨Shadows, lights and other post processing effects

🟨Mesh animations

🟨Rendering transparent objects

🟨DirectX12 support
