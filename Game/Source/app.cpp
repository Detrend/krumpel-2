#include <Krumpel.h>
#include <chrono>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <filesystem>

using namespace kr;

INT WINAPI wWinMain(HINSTANCE hThisInstance, HINSTANCE hPrevInstance, PWSTR lpszArgument, int iCmdShow)
{
	cCore* Engine = new cCore();
	Engine->Init();		
	Engine->Run();
	Engine->Terminate();
	delete Engine;
	ExitProcess(0);
}