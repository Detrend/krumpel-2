#pragma once

#include <Windows.h>
#include "Basics.h"

KR_BEGIN

LRESULT CALLBACK DefaultWindowProcedure(HWND win, UINT msg, WPARAM wparam, LPARAM lparam);

typedef LRESULT(CALLBACK* tWindowProcedure)(HWND, UINT, WPARAM, LPARAM);

KR_END