// This includes all global variables we might need

#pragma once
#include <Windows.h>
#include "Basics.h"
#include "Glew.h"

KR_BEGIN

// To disable FPS cap and vertical sync
inline PFNWGLSWAPINTERVALEXTPROC       wglSwapIntervalEXT = nullptr;
inline PFNWGLGETSWAPINTERVALEXTPROC    wglGetSwapIntervalEXT = nullptr;

KR_END