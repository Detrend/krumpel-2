#pragma once
#include <utility>
#include <functional>
#include <string>
#include <memory>
#include <stb/stb_image.h>

#include "Glew.h"
#include "Basics.h"


KR_BEGIN

class cTexture : public iNullReference
{
private:
	std::unique_ptr<byte, std::function<void(byte*)>> mData;		// pointer to data with custom deleter
    uint32 mWidth;
	uint32 mHeight;
	uint32 mChannels;

	GLuint mHandle;		// OpenGL handle
	bool mInGpu;

private:
	static void FreeTextureData(byte* Data);

public:
	cTexture();
	
	cTexture(cTexture&&);

	cTexture(const cTexture&) = delete;

	cTexture& operator=(const cTexture&) = delete;
	
	~cTexture();

	cTexture& operator=(cTexture&&);

	void FreeData();

	void UploadToGpu();

	void FreeFromGpu();

	bool InGpu() const;

	GLuint GetHandle() const;

	eResult LoadFromFile(const char* File);

	uint GetWidth() const;

	uint GetHeight() const;

	uint GetChannels() const;

	byte* GetData() const;

	virtual bool IsNull() const override;
};

inline const cTexture NullTexture;

KR_END