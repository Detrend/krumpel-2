#pragma once

#include "Basics.h"
#include "Glew.h"

class iGlHandle
{
public:
    GLuint GetHandle() const {};
};