#pragma once

#include <string>
#include <memory>
#include <glm/glm.hpp>
#include <utility>
#include <tuple>

#include "Glew.h"
#include "Basics.h"
#include "Texture.h"
#include "Mesh.h"

KR_BEGIN

class cGenerator
{
public:
	static void GenVertexArray(GLuint& ID);

	static GLuint GenVertexArray();

	static void GenVertexBuffer(GLuint& ID, uint32 Size, void* Data, GLenum Usage = GL_STATIC_DRAW);

	static GLuint GenVertexBuffer(uint32 Size, void* Data, GLenum Usage = GL_STATIC_DRAW);

	static void SetVertexBuffer(GLuint& ID, uint32 Size, void* Data, GLenum Usage = GL_STATIC_DRAW);

	static void GenIndexBuffer(GLuint& ID, uint32 Count, void* Data, GLenum Usage = GL_STATIC_DRAW);

	static GLuint GenIndexBuffer(uint32 Count, void* Data, GLenum Usage = GL_STATIC_DRAW);

	static void SetIndexBuffer(GLuint& ID, uint32 Count, void* Data, GLenum Usage = GL_STATIC_DRAW);

	static void SetAttribute(GLuint VertexArrayID, uint32 Index, uint32 Count, uint32 Stride, uint64 Offset, GLenum Type = GL_FLOAT, bool Normalized = false);

	static GLuint GenTexture();

	static void GenTexture(GLuint& ID);

	static void SetTexture(GLuint ID, cTexture& Texture);

	static cMesh GenPlane(std::pair<float, float> UpLeftCorner, std::pair<float, float> BottomRightCorner, float Height, cTexture& Texture);

	static cMesh GenCube(std::tuple<float, float, float> FrontTopLeftCorner,
						  std::tuple<float, float, float> BottomBackRightCorner);

	static cMesh GenScreen();
};

KR_END