#pragma once

#include "Glew.h"
#include "Basics.h"
#include "GlHandle.h"

KR_BEGIN

// Maximum number of components/textures each framebuffer can contain
constexpr uint32 FRAMEBUFFER_TEXTURE_COMPONENTS = 8;

class cFramebuffer : public iGlHandle
{
    private:
        GLuint mFramebufferHandle;  
        GLuint mDepthBufferHandle;
        GLuint mTextureHandles[FRAMEBUFFER_TEXTURE_COMPONENTS];
        uint32 mTexturesCount;
        bool   mValid;

    private:
        Result<> Free();

    public:
        cFramebuffer() : mFramebufferHandle(0), mDepthBufferHandle(0), mTextureHandles(), mTexturesCount(0), mValid(false) {}
        ~cFramebuffer();

        // Binds the framebuffer. Consequent OpenGL calls will use this framebuffer.
        // Framebuffer must be valid
        Result<> Bind();

        // Returns number of textures this framebuffer contains
        uint32 GetTexturesCount()               const { return mTexturesCount;          }
        // Returns OpenGL handle of this framebuffer
        GLuint GetFramebufferHandle()           const { return mFramebufferHandle;      }
        // Returns OpenGL handle of this framebuffer. Same as 'GetFramebufferHandle'
        GLuint GetHandle()                      const { return mFramebufferHandle;      }
        // Returns true if this framebuffer was created correctly
        bool IsValid()                          const { return mValid;                  }
        // Returns texture of the framebuffer with index 'Index'.
        // Index must be smaller than value returned by 'GetTexturesCount'
        GLuint GetTextureHandle(uint32 Index)   const { return mTextureHandles[Index];  }
        // Creates a framebuffer with desired 'Width', 'Height' and number of MSAA samples.
        // 'CountNotZero' must be higher than zero and smaller than 'FRAMEBUFFER_TEXTURE_COMPONENTS'.
        // Frees and erases the old data.
        Result<> Create(uint32 Width, uint32 Height, uint32 MSAA, uint32 CountNotZero = 1);
};

KR_END