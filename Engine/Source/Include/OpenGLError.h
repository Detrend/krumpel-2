//
//	DEPRECATED!!!
//
#pragma once

#include "Basics.h"
#include "Basics.h"
#include "Logger.h"

#include <iostream>

KR_BEGIN

class cOpenGLError
{
public:
	static void ClearErrorQueue();

	static void CheckError();
};

#define PrintGlError(err) Logger << "OpenGLError: " << ColorRed << #err << ColorWhite << Endl

#define krGlCall(call) cOpenGLError::ClearErrorQueue(); call; cOpenGLError::CheckError();

KR_END