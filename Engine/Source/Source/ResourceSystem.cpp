#include "ResourceSystem.h"

KR_BEGIN

ResourceSystem::ResourceSystem()
{
    
}

ResourceSystem::~ResourceSystem()
{

}

ResourceSystem& ResourceSystem::Instance()
{
    static ResourceSystem MyInstance;
    return MyInstance;
}

const cMesh& ResourceSystem::GetMesh(cMeshHandle aHandle)
{
    if (aHandle.Index >= mMeshStorage.size() or aHandle.Index < 0)
    {
        return NullMesh;
    }
    else
    {
        auto& Cell = mMeshStorage[aHandle.Index];
        if (Cell.Key == aHandle.Key)
        {
            return Cell.Data;
        }
        else
        {
            return NullMesh;
        }
    }
}

const cTexture& ResourceSystem::GetTexture(cTextureHandle aHandle)
{
    if (aHandle.Index >= mMeshStorage.size() or aHandle.Index < 0)
    {
        return NullTexture;
    }
    else
    {
        auto& Cell = mTextureStorage[aHandle.Index];
        if (Cell.Key == aHandle.Key)
        {
            return Cell.Data;
        }
        else
        {
            return NullTexture;
        }
    }
}

const cShader& ResourceSystem::GetShader(cShaderHandle aHandle)
{
    if (aHandle.Index >= mMeshStorage.size() or aHandle.Index < 0)
    {
        return NullShader;
    }
    else
    {
        auto& Cell = mShaderStorage[aHandle.Index];
        if (Cell.Key == aHandle.Key)
        {
            return Cell.Data;
        }
        else
        {
            return NullShader;
        }
    }
}

eResult ResourceSystem::LoadMesh(const char* aFilePath, cMeshHandle& aHandle)
{
    cMeshHandle Handle;

    // Check if this resource was already loaded
    if (MeshAlreadyLoaded(aFilePath, Handle))
    {
        aHandle = Handle;
        return {RESULT_OK};
    }

    const int Key = 0;
    const int Index = static_cast<int>(mMeshStorage.size());
    MeshCell& Cell = mMeshStorage.emplace_back();

    // Load mesh from file
    if (Failed(Cell.Data.LoadFromFile(aFilePath)))
    {
        mMeshStorage.pop_back();        
        return {RESULT_FAIL};
    }

    Cell.Path = std::move(std::filesystem::absolute(aFilePath));
    Cell.Data.UploadToGpu();
    Cell.Key = Key;

    Handle.Index = Index;
    Handle.Key = Key;

    mLoadedMeshes.insert({Cell.Path, Handle});

    aHandle = Handle;
    return {RESULT_OK};
}

eResult ResourceSystem::LoadTexture(const char* aFilePath, cTextureHandle& aHandle)
{
    const int Key = 0;
    const int Index = static_cast<int>(mTextureStorage.size());

    TextureCell& Cell = mTextureStorage.emplace_back();
    Cell.Data.LoadFromFile(aFilePath);
    Cell.Data.UploadToGpu();
    Cell.Key = Key;

    cTextureHandle Handle;
    Handle.Index = Index;
    Handle.Key = Key;

    aHandle = Handle;
    return eResult::RESULT_OK;
}

eResult ResourceSystem::LoadShader(const char* aFilePath, cShaderHandle& aHandle)
{
    const int Key = 0;
    const int Index = static_cast<int>(mShaderStorage.size());

    ShaderCell& Cell = mShaderStorage.emplace_back();
    if (Failed(Cell.Data.LinkFromFile(aFilePath)))
    {
        return eResult::RESULT_FAIL;
    }

    Cell.Key = Key;

    cShaderHandle Handle;
    Handle.Index = Index;
    Handle.Key = Key;

    aHandle = Handle;
    return eResult::RESULT_OK;
} 

bool ResourceSystem::MeshAlreadyLoaded(const char* aFilePath, cMeshHandle& aHandle)
{
    const std::filesystem::path Path = std::move(std::filesystem::absolute(std::move(aFilePath)));
    if (mLoadedMeshes.count(Path))
    {
        aHandle = mLoadedMeshes[Path];
        return true;
    }
    else
    {
        return false;
    }
}

bool ResourceSystem::TextureAlreadyLoaded(const char* aFilePath, cTextureHandle& aHandle)
{
    const std::filesystem::path Path = std::move(std::filesystem::absolute(std::move(aFilePath)));
    if (mLoadedTextures.count(Path))
    {
        aHandle = mLoadedTextures[Path];
        return true;
    }
    else
    {
        return false;
    } 
}

bool ResourceSystem::ShaderAlreadyLoaded(const char* aFilePath, cShaderHandle& aHandle)
{
    const std::filesystem::path Path = std::move(std::filesystem::absolute(std::move(aFilePath)));
    if (mLoadedMeshes.count(Path))
    {
        aHandle = mLoadedShaders[Path];
        return true;
    }
    else
    {
        return false;
    }
}

KR_END
