#include <Windows.h>
#include <iostream>
#include <cstdio>

#include "Glew.h"
#include "Logger.h"

KR_BEGIN

cLog::cLog()
    : mFile(), mEngineSeverity(ReportWarnings), mOpenGlSeverity(ReportWarnings) {}

void GLAPIENTRY cLog::GlDebugCallback(GLenum aSource, GLenum aType, GLuint aID, GLenum aSeverity, GLsizei aLength, const GLchar* aMessage, const void* aUserParam)
{
    const char* Source;
    switch (aSource) {
    case GL_DEBUG_SOURCE_API:
        Source = "OpenGl - API";
        break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
        Source = "OpenGl - Window system";
        break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER:
        Source = "OpenGl - Shader compiler";
        break;
    case GL_DEBUG_SOURCE_THIRD_PARTY:
        Source = "OpenGl - Third party";
        break;
    case GL_DEBUG_SOURCE_APPLICATION:
        Source = "OpenGl - Application";
        break;
    case GL_DEBUG_SOURCE_OTHER:
        Source = "OpenGl - Other";
        break;
    default:
        Source = "OpenGl - Unknown";
        break;
    }

    const char* Type;
    switch (aType) {
    case GL_DEBUG_TYPE_ERROR:
        Type = "Error";
        break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        Type = "Deprecated";
        break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        Type = "Undefined";
        break;
    case GL_DEBUG_TYPE_PORTABILITY:
        Type = "Portability";
        break;
    case GL_DEBUG_TYPE_PERFORMANCE:
        Type = "Performance";
        break;
    case GL_DEBUG_TYPE_OTHER:
        Type = "Other";
        break;
    case GL_DEBUG_TYPE_MARKER:
        Type = "Marker";
        break;
    default:
        Type = "Unknown";
        break;
    }

    int BufferSize = aLength + 30;
    char* OutputMessage = new char[BufferSize];
    snprintf(OutputMessage, BufferSize, "(%s) %s", Type, aMessage);

    switch (aSeverity) {
    case GL_DEBUG_SEVERITY_HIGH:
        Logger.ErrorOpenGl(OutputMessage, Source);
        break;
    case GL_DEBUG_SEVERITY_MEDIUM:
        Logger.WarningOpenGl(OutputMessage, Source);
        break;
    case GL_DEBUG_SEVERITY_LOW:
        Logger.WarningOpenGl(OutputMessage, Source);
        break;
    case GL_DEBUG_SEVERITY_NOTIFICATION:
        Logger.NotifyOpenGl(OutputMessage, Source);
        break;
    }

    delete[] OutputMessage;
}

cLog::cLog(const std::string& aFileName)
    : mFile(), mEngineSeverity(ReportWarnings), mOpenGlSeverity(ReportWarnings)
{
    Open(aFileName);
}

cLog::cLog(const char* aFileName)
    : mFile(), mEngineSeverity(ReportWarnings), mOpenGlSeverity(ReportWarnings)
{
    Open(aFileName);
}

cLog::cLog(std::ostream& aOutput) 
    : mFile(), mEngineSeverity(ReportWarnings), mOpenGlSeverity(ReportWarnings)
{
    Open(aOutput);
}

Result<void> cLog::Open(const std::string& aFileName)
{
    return Open(aFileName.c_str());
}

Result<void> cLog::Open(const char* aFileName)
{
    if (mFile.is_open() && mFileOpenedByMe)
    {
        mFile.close();
    }

    mFile.open(aFileName, std::ios::out | std::ios::app);
    mFileOpenedByMe = true;

    if (mFile.is_open())
    {
        return {RESULT_OK};
    }
    else
    {
        return {RESULT_NO_ACCESS};
    }
}

Result<void> cLog::Open(std::ostream& aOutput)
{
    if (mFile.is_open() && mFileOpenedByMe)
    {
        mFile.close();
    }

    mFile.basic_ios<char>::rdbuf(aOutput.rdbuf());
    mFileOpenedByMe = false;

    return {RESULT_OK};
}

cLog::~cLog()
{
    if (mFile.is_open()) {
        mFile.flush();

        if (mFileOpenedByMe) {
            mFile.close();
        }
    }
}

cLog& cLog::Print(const char* aString)
{
    mFile << aString;

    return *this;
}

cLog& cLog::Print(const cPrintable& aPrintable)
{
    mFile << aPrintable.Represent().c_str();
    
    return *this;
}

cLog& cLog::PrintWinError(DWORD Err)
{
    LPSTR messageBuffer = nullptr;

    std::size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL, Err, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

    std::string message(messageBuffer, size);
    LocalFree(messageBuffer);
    Print(message.c_str());

    return *this;
}

cLog& cLog::operator<<(const char* aString)
{
    return Print(aString);
}

cLog& cLog::operator<<(const cPrintable& aPrintable)
{
    return Print(aPrintable);
}

void cLog::SetSeverityLevel(LoggerSeverityLevel aLevel)
{
    mEngineSeverity = aLevel;
    Print(ColorWhite).Print("[").Print(ColorGray).Print("Logger").Print(ColorWhite).Print("]").Print(ColorGray).Print("Severity level set to '");
    switch (aLevel) {
        case LoggerSeverityLevel::ReportNothing: Print(ColorWhite).Print("Nothing"); break;
        case LoggerSeverityLevel::ReportWarnings: Print(ColorYellow).Print("Warnings and errors"); break;
        case LoggerSeverityLevel::ReportOnlyCritical: Print(ColorRed).Print("Only errors"); break;
        case LoggerSeverityLevel::ReportEverything: Print(ColorGreen).Print("Everyting"); break;
    }
    Print(ColorGray).Print("'.").Print(Endl);
}

void cLog::SetOpenGlSeverityLevel(LoggerSeverityLevel aLevel)
{
    mOpenGlSeverity = aLevel;
    Print(ColorWhite).Print("[").Print(ColorGray).Print("Logger").Print(ColorWhite).Print("]").Print(ColorGray).Print("OpenGl severity level set to '");
    switch (aLevel) {
        case LoggerSeverityLevel::ReportNothing: Print(ColorWhite).Print("Nothing"); break;
        case LoggerSeverityLevel::ReportWarnings: Print(ColorYellow).Print("Warnings and errors"); break;
        case LoggerSeverityLevel::ReportOnlyCritical: Print(ColorRed).Print("Only errors"); break;
        case LoggerSeverityLevel::ReportEverything: Print(ColorGreen).Print("Everyting"); break;
    }
    Print(ColorGray).Print("'.").Print(Endl);
}

LoggerSeverityLevel cLog::GetSeverityLevel()
{
    return mEngineSeverity;
}

LoggerSeverityLevel cLog::GetOpenGlSeverityLevel()
{
    return mOpenGlSeverity;
}

void cLog::Notify(const char* aMessage, const char* aSource)
{
    if (mEngineSeverity == ReportEverything) {
        Print("[").Print(ColorGray).Print("Note").Print(ColorWhite).Print("]");
        if (aSource != nullptr) {
            Print("[").Print(ColorGray).Print(aSource).Print(ColorWhite).Print("]");
        }
        Print(aMessage).Print(Endl);
    }
}

void cLog::NotifyOpenGl(const char* aMessage, const char* aSource)
{
    if (mOpenGlSeverity == ReportEverything) {
        Print("[").Print(ColorGray).Print("Note").Print(ColorWhite).Print("]");
        if (aSource != nullptr) {
            Print("[").Print(ColorGray).Print(aSource).Print(ColorWhite).Print("]");
        }
        Print(aMessage).Print(Endl);
    }
}

void cLog::Warning(const char* aMessage, const char* aSource)
{
    if (mEngineSeverity != ReportOnlyCritical and mEngineSeverity != ReportNothing) {
        Print("[").Print(ColorYellow).Print("Warning").Print(ColorWhite).Print("]");
        if (aSource != nullptr) {
            Print("[").Print(ColorGray).Print(aSource).Print(ColorWhite).Print("]");
        }
        Print(aMessage).Print(Endl);
    }
}

void cLog::WarningOpenGl(const char* aMessage, const char* aSource)
{
    if (mOpenGlSeverity != ReportOnlyCritical and mOpenGlSeverity != ReportNothing) {
        Print("[").Print(ColorYellow).Print("Warning").Print(ColorWhite).Print("]");
        if (aSource != nullptr) {
            Print("[").Print(ColorGray).Print(aSource).Print(ColorWhite).Print("]");
        }
        Print(aMessage).Print(Endl);
    }
}

void cLog::Error(const char* aMessage, const char* aSource)
{
    if (mEngineSeverity != ReportNothing) {
        Print("[").Print(ColorRed).Print("Error").Print(ColorWhite).Print("]");
        if (aSource != nullptr) {
            Print("[").Print(ColorGray).Print(aSource).Print(ColorWhite).Print("]");
        }
        Print(aMessage).Print(Endl);
    }
}

void cLog::ErrorOpenGl(const char* aMessage, const char* aSource)
{
    if (mOpenGlSeverity != ReportNothing) {
        Print("[").Print(ColorRed).Print("Error").Print(ColorWhite).Print("]");
        if (aSource != nullptr) {
            Print("[").Print(ColorGray).Print(aSource).Print(ColorWhite).Print("]");
        }
        Print(aMessage).Print(Endl);
    }
}

void cLog::WinError(DWORD aErr, const char* aSource)
{
    if (mEngineSeverity != ReportNothing) {
        Print("[").Print(ColorRed).Print("Error").Print(ColorWhite).Print("]");
        if (aSource != nullptr) {
            Print("[").Print(ColorGray).Print(aSource).Print(ColorWhite).Print("]");
        }
        PrintWinError(aErr).Print(Endl);
    }
}

eResult cLog::EnableOpenGlDebugLog()
{
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(GlDebugCallback, 0);

    return eResult::RESULT_OK;
}

KR_END