#pragma once

#define _CRT_SECURE_NO_WARNINGS

#define GLEW_STATIC
#include <glew.h>
#include <glm/glm.hpp>
#include <stb/stb_image.h>

#include "Basics.h"

#include "OpenGLError.h"

#include "Printable.h"
#include "LoggerUtil.h"
#include "Logger.h"

#include "Window.h"
#include "Utility.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "Core.h"

#include "Shader.h"
#include "Renderer.h"
#include "Generator.h"
#include "Buffer.h"
#include "IndexBuffer.h"
#include "VertexBuffer.h"
#include "Mesh.h"
#include "Texture.h"

#include "Generator.h"
#include "Framebuffer.h"

#include "Object.h"
#include "Scene.h"
#include "Camera.h"