#include "Camera.h"
#include "glm/gtc/matrix_transform.hpp"

KR_BEGIN

cCamera::cCamera()
	: mFrustumMat(1.0f)
{}

cCamera::cCamera(float aFov, float aAspect, float aNear, float aFar)
	: mViewFov(aFov), mViewAspect(aAspect), mViewNear(aNear), mViewFar(aFar)
{
	ResetProjection();
}

void cCamera::ResetProjection()
{
	mFrustumMat = glm::perspective(glm::radians(mViewFov), mViewAspect, mViewNear, mViewFar);
}

void cCamera::SetProjection(float aFOV, float aAspect, float aNear, float aFar)
{
	mViewFov = aFOV; mViewAspect = aAspect; mViewNear = aNear; mViewFar = aFar;
	ResetProjection();
}

glm::mat4 cCamera::GetWorldToView() const
{
	return glm::lookAt(GetPosition(), GetPosition() - glm::vec3(GetTransform()[2]), glm::vec3(GetTransform()[1]));
}

glm::mat4 cCamera::GetViewToWorld() const
{
	return glm::inverse(GetWorldToView());
}

maths::Ray cCamera::GetRayInWorld(glm::vec3 ScreenSpace)
{
	glm::vec4 origin = GetViewToWorld() * glm::inverse(GetProjection()) * glm::vec4(ScreenSpace.x, ScreenSpace.y, ScreenSpace.z, 1.0f);
	origin.w = 1.0f / origin.w;
	origin *= origin.w;
	return maths::Ray{GetPosition(), glm::vec3(origin) - GetPosition()};
}

KR_END