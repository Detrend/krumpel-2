#include <Windows.h>
#include <cstdlib>
#include <cstdio>

#include "Glew.h"
#include "Basics.h"
#include "Window.h"
#include "Core.h"
#include "Mouse.h"
#include "Keyboard.h"

KR_BEGIN

cWindow::cWindow() : mIsInitialized(false) {}

Result<> cWindow::Init(cCore* aCore, const wchar_t* aWindowName,
                      int aWindowWidth,
                      int aWindowHeight,
                      bool aFullscreen) 
{
    if (aCore == nullptr) {
        Logger.Error("Could not initialize window. Pointer to core was null", "Window");
        return {RESULT_NULL_ARGUMENT};
    }

    HINSTANCE ProgramInstance = GetModuleHandle(nullptr);
    mCore = aCore;

    // Prevent double initialization
    if (mIsInitialized) {
        Logger.Error("Window was already initialized before", "Window init");
        return {RESULT_UNEXPECTED};
    }

    // WinAPI needs us to create 'window class', which specifies the parameters of our window
    auto Win32ClassSetup = SetupWin32Class(ProgramInstance);
    if (Failed(Win32ClassSetup)) {
        Logger.Error("Failed to initialize win32 class", "Window init");
        return {RESULT_INIT_FAIL};
    }

    // Create window and retrieve its handle. The last argument is pointer to this insance of 'cWindow' class,
    // which we will need to access from window procedure
    mHandle = CreateWindowExW(NULL,
                              DEFAULT_WINDOW_CLASS_NAME,
                              aWindowName,
                              WS_OVERLAPPEDWINDOW,
                              0, 0,
                              aWindowWidth,
                              aWindowHeight,
                              NULL, NULL, ProgramInstance, this);

    if (mHandle == NULL) {
        // Failed to create the window, report the error
        DWORD Err = GetLastError();
        Logger.WinError(Err, "Window initialization");
        return {RESULT_INIT_FAIL};
    } else {
        ShowWindow(mHandle, SW_MAXIMIZE);
        UpdateWindow(mHandle);
        auto OpenGlInit = InitOpenGl();
        if (Failed(OpenGlInit)) {
            Logger.Error("Failed to initialize OpenGL", "Window init");
            return {RESULT_FAIL};
        }

        if (aFullscreen)
            SetFullscreen();

        mIsInitialized = true;

        return {RESULT_OK};
    }
}

cWindow::~cWindow()
{
    if (mHasRenderingContext) {
        TerminateOpenGl();
    }
}

Result<>cWindow::Init(cCore* aCore, const char* aWindowName, int aWindowWidth, int aWindowHeight, bool aFullscreen)
{
    auto Retval = Init(aCore, L"", aWindowWidth, aWindowHeight, aFullscreen);
    SetCaption(aWindowName);
    return Retval;
}

HWND cWindow::GetHandle() const
{
    return mHandle;
}

HDC cWindow::GetDeviceContext() const
{
    return mDeviceContext;
}

HGLRC cWindow::GetRenderingContext() const
{
    return mRenderingContext;
}

int cWindow::PeekMsg() {
    if (mMessage.message == WM_QUIT)
        mClosed = TRUE;
    return PeekMessageW(&mMessage, NULL, 0, 0, PM_REMOVE);
}

int cWindow::ProcessMsg() {
    TranslateMessage(&mMessage);						//	adds additional messages to queue
    DispatchMessageW(&mMessage);						//	calls cWindow proc
    return 0;
}

void cWindow::ProcessMessages()
{
    while (PeekMsg())
        ProcessMsg();
}

int cWindow::IsClosed()
{
    return mClosed;
}

Result<> cWindow::Close()
{
    mClosed = true;
    return {RESULT_OK};
}

Result<> cWindow::SetFullscreenState(bool aState)
{
    mFullscreen = aState;
    SetWindowLongPtr(mHandle, GWL_STYLE, WS_VISIBLE | (aState ? WS_POPUP : WS_OVERLAPPEDWINDOW));
    SendMessage(mHandle, WM_SYSCOMMAND, SC_MAXIMIZE, 0);
    wglMakeCurrent(mDeviceContext, mRenderingContext);

    RECT cWindow_size;
    GetClientRect(mHandle, &cWindow_size);
    glViewport(0, 0, cWindow_size.right, cWindow_size.bottom);

    return {RESULT_OK};
}

void cWindow::InitOpenGlPixelFormatDescritor(PIXELFORMATDESCRIPTOR& aPfd)
{
    std::memset(&aPfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
    aPfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    aPfd.nVersion = 1;
    aPfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    aPfd.iPixelType = PFD_TYPE_RGBA;
    aPfd.cColorBits = 32;
    aPfd.cDepthBits = 32;
    aPfd.cStencilBits = 8;
    aPfd.iLayerType = PFD_MAIN_PLANE;
}

Result<> cWindow::SetFullscreen()
{
    SetFullscreenState(true);
    return {RESULT_OK};
}

Result<> cWindow::SetWindowed()
{
    SetFullscreenState(false);
    return {RESULT_OK};
}

Result<> cWindow::SetCaption(const wchar_t* aText)
{
    if (SetWindowTextW(mHandle, aText) == 0) {
        return {RESULT_FAIL};
    } else {
        return {RESULT_OK};
    }
}

Result<> cWindow::SetCaption(const char* aText)
{
    if (SetWindowTextA(mHandle, aText) == 0) {
        return {RESULT_FAIL};
    } else {
        return {RESULT_OK};
    }
}

bool cWindow::IsFullscreen()
{
    return mFullscreen;
}

Result<> cWindow::SetKeyboard(cKeyboard* keyboard)
{
    mKeyboard = keyboard;

    return {RESULT_OK};
}

Result<> cWindow::SetMouse(cMouse* mouse)
{
    mMouse = mouse;

    return {RESULT_OK};
}

Result<> cWindow::InitOpenGl()
{
    PIXELFORMATDESCRIPTOR pfd;
    cWindow::InitOpenGlPixelFormatDescritor(pfd);
    
    mDeviceContext = GetDC(mHandle);
    int pxFormat = ChoosePixelFormat(mDeviceContext, &pfd);
    if (!SetPixelFormat(mDeviceContext, pxFormat, &pfd) or !pxFormat) {
        Logger.Error("Failed to set pixel format descriptor", "OpenGL init");
        return {RESULT_INIT_FAIL};
    }

    // temporary context creation
    auto TempContext = wglCreateContext(mDeviceContext);
    if (!wglMakeCurrent(mDeviceContext, TempContext)) {
        Logger.Error("Failed to create temporary context", "OpenGL init");
        return {RESULT_INIT_FAIL};
    }

    // Initialize GLEW
    if (glewInit() != GLEW_OK) {
        Logger.Error("Failed to init GLEW", "OpenGL init");
        return {RESULT_INIT_FAIL};
    }

    // Advanced context creation (for OpenGL 3.2)
    if (wglewIsSupported("WGL_ARB_create_context")) {
        // Attributes of advanced context. Specifies our requirements of the context to OpenGl
        int ContextAttributes[] = {
            WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
            WGL_CONTEXT_MINOR_VERSION_ARB, 2,
            WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
            0};

        mRenderingContext = wglCreateContextAttribsARB(mDeviceContext, 0, ContextAttributes);
        if (mRenderingContext == NULL) {
            Logger.Error("Failed to create core OpenGL context", "OpenGL init");
            return {RESULT_INIT_FAIL};
        }

        // Delete temp context
        wglMakeCurrent(NULL, NULL);
        wglDeleteContext(TempContext);

        // Apply the new context
        if (!wglMakeCurrent(mDeviceContext, mRenderingContext)) {
            Logger.Error("Failed to create core OpenGL context", "OpenGL init");
            return {RESULT_INIT_FAIL};
        }
    } else {
        mRenderingContext = TempContext;
    }

    mHasRenderingContext = true;

    return {RESULT_OK};
}

Result<> cWindow::SetupWin32Class(HINSTANCE aProgramHandle)
{
    WNDCLASS DefaultWindowClass;
    DefaultWindowClass.lpszClassName = DEFAULT_WINDOW_CLASS_NAME;
    DefaultWindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    DefaultWindowClass.cbClsExtra = 0;
    DefaultWindowClass.cbWndExtra = sizeof(cWindow*);
    DefaultWindowClass.lpfnWndProc = DefaultWindowProcedure;
    DefaultWindowClass.hInstance = aProgramHandle;
    DefaultWindowClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    DefaultWindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    DefaultWindowClass.hbrBackground = (HBRUSH)(COLOR_BACKGROUND + 1);
    DefaultWindowClass.lpszMenuName = NULL;

    if (!RegisterClassW(&DefaultWindowClass)) {
        // Failed to register window class
        DWORD Err = GetLastError();
        Logger.WinError(Err, "Window class setup");
        return {RESULT_INIT_FAIL};
    } else {
        return {RESULT_OK};
    }
}

Result<> cWindow::TerminateOpenGl()
{
    if (!mHasRenderingContext) {
        Logger.Warning("Window tries to terminate OpenGL context, but the context was not initialized in the first place", "Window");
        return {RESULT_UNEXPECTED};
    }

    mHasRenderingContext = false;
    BOOL Result = wglDeleteContext(mRenderingContext);
    if (Result == TRUE)
        return {RESULT_OK};
    else
        return {RESULT_FAIL};
}

int cWindow::GetWidth() const
{
    return mWidth;
}

int cWindow::GetHeight() const
{
    return mHeight;
}

void cWindow::BindContext()
{
    wglMakeCurrent(mDeviceContext, mRenderingContext);
}

void cWindow::SetVsync(bool aOn)
{
    if (wglSwapIntervalEXT != nullptr)
        wglSwapIntervalEXT(aOn);
}

bool cWindow::HasWinVsync()
{
    if (wglGetSwapIntervalEXT != nullptr)
        return wglGetSwapIntervalEXT();
    return true;
}

void cWindow::Swap()
{
    SwapBuffers(mDeviceContext);
}

KR_END