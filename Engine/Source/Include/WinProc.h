#pragma once

#include <Windows.h>

#include "Basics.h"

KR_BEGIN

class cWindow;


class cWinProc
{
public:
	static void StoreWindowPointer(HWND Handle, cWindow* PointerToBaseClass);

	static cWindow* GetWindowPointer(HWND Handle);

	static void ResizeWindow(cWindow* Window);
};

KR_END