// engine macros
#pragma once

#define KR_BEGIN	namespace kr {
#define KR_END		}

#define KR_BREAKPOINT __debugbreak()        

#define KR_ASSERT(exp) if (!(exp)) __debugbreak();

#ifdef KR_TESTING
#define KR_PRIVATE public
#define KR_PROTECTED public
#define KR_PUBLIC public
#else
#define KR_PRIVATE private
#define KR_PROTECTED protected
#define KR_PUBLIC public
#endif