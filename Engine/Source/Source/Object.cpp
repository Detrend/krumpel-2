#include "Object.h"
#include "glm/gtx/matrix_decompose.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/quaternion.hpp"
#include "glm/gtx/euler_angles.hpp"
#include "glm/gtc/matrix_access.hpp"

KR_BEGIN

cObject::cObject() : mObjectId(ObjectCounter++), mIsSelected(false) {}

cObject::~cObject() {}

void cObject::SetPosition(glm::vec3 aNewPosition)
{
    glm::mat4 Back = glm::translate(glm::mat4(1.0f), -GetPosition());		    // move to origin
    glm::mat4 ToNew = glm::translate(glm::mat4(1.0f), aNewPosition);			// move to new location
    mObjectTransform = ToNew * Back * mObjectTransform;
}

glm::vec3 cObject::GetPosition() const
{
    return glm::vec3(glm::column(mObjectTransform, 3));
}

void cObject::SetScale(glm::vec3 aScale)
{
    glm::vec3 Pos = GetPosition();                                          // get last position
    glm::mat4 Rot = glm::mat4_cast(glm::quat_cast(mObjectTransform));       // get rotation
    mObjectTransform = glm::mat4(1.0f);                                     // reset
    mObjectTransform = glm::scale(mObjectTransform, aScale);                // rescale
    mObjectTransform = Rot * mObjectTransform;                              // rotate back
    mObjectTransform = glm::translate(mObjectTransform, Pos);               // move back
}

glm::vec3 cObject::GetScale() const
{
    glm::vec3 Scale;
    for (int i = 0; i < 3; i++) {
        Scale[i] = glm::length(glm::vec3(mObjectTransform[i]));
    }
    return Scale;
}

void cObject::Transform(const glm::mat4& aTransformationMatrix)
{
    mObjectTransform = aTransformationMatrix * mObjectTransform;
}

void cObject::SetTransform(const glm::mat4& aTransformationMatrix)
{
    mObjectTransform = aTransformationMatrix;
}

const glm::mat4& cObject::GetTransform() const
{
    return mObjectTransform;
}

void cObject::RotateX(float aAngle)
{
    mObjectTransform = glm::rotate(glm::mat4(1.0f), glm::radians(aAngle), {1.0f, 0.0f, 0.0f}) * mObjectTransform;
}

void cObject::RotateY(float aAngle)
{
    mObjectTransform = glm::rotate(glm::mat4(1.0f), glm::radians(aAngle), {0.0f, 1.0f, 0.0f}) * mObjectTransform;
}

void cObject::RotateZ(float aAngle)
{
    mObjectTransform = glm::rotate(glm::mat4(1.0f), glm::radians(aAngle), {0.0f, 0.0f, 1.0f}) * mObjectTransform;
}

glm::vec3 cObject::GetRotationAngles() const
{
    glm::vec3 Euler = glm::eulerAngles(glm::quat_cast(mObjectTransform));
    return Euler;
}

glm::mat3 cObject::GetRotationMatrix() const
{
    return glm::mat3(mObjectTransform);
}

void cObject::SetRotationAngles(glm::vec3 aRotationXYZ)
{
    glm::vec3 Scale = GetScale();
    glm::vec3 Pos = GetPosition();
    mObjectTransform = glm::rotate(glm::mat4(1.0f), glm::radians(aRotationXYZ.z), glm::vec3(0.0f, 0.0f, 1.0f));
    mObjectTransform = glm::rotate(mObjectTransform, glm::radians(aRotationXYZ.x), glm::vec3(1.0f, 0.0f, 0.0f));
    mObjectTransform = glm::rotate(mObjectTransform, glm::radians(aRotationXYZ.y), glm::vec3(0.0f, 1.0f, 0.0f));
    mObjectTransform = glm::scale(mObjectTransform, Scale);
    mObjectTransform = glm::translate(mObjectTransform, Pos);
}

void cObject::SetRotationMatrix(const glm::mat3& aRotation)
{
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            mObjectTransform[i][j] = aRotation[i][j];
        }
    }
}

cObject& cObject::AddChild()
{
    return *mChildren.emplace_back(std::make_shared<cObject>());
}

const std::list<std::shared_ptr<cObject>>& cObject::GetChildren() const
{
    return mChildren;
}

std::list<std::shared_ptr<cObject>>& cObject::GetChildren()
{
    return mChildren;
}

cMeshHandle cObject::GetMesh() const
{
    return mMesh;
}

void cObject::SetMesh(cMeshHandle aHandle)
{
    mMesh = aHandle;	
}

int cObject::GetId() const
{
    return mObjectId;
}

cShaderHandle cObject::GetMaterial() const
{
    return mMaterial;
}

void cObject::SetMaterial(cShaderHandle aHandle)
{
    mMaterial = aHandle;
}

cTextureHandle cObject::GetTexture() const
{
    return mTexture;
}

void cObject::SetTexture(cTextureHandle aHandle)
{
    mTexture = aHandle;	
}

bool cObject::IsSelected() const
{
    return mIsSelected;
}

void cObject::SetSelectedState(bool aState)
{
    mIsSelected = aState;
}

KR_END