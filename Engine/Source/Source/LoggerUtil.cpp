#include <Windows.h>

#include "LoggerUtil.h"

KR_BEGIN

std::string cPrintableCharacter::Represent() const
{
	return std::string(mCharacters);
}

std::string cPrintableColor::Represent() const
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), mColorId);
	return std::string("");
}

KR_END
