//  Error handling functionality
#pragma once
#include "Basics.h"

KR_BEGIN

// Enum for various return value results
enum eResult
{
    RESULT_OK = 0,              // = success
    RESULT_FAIL = 1,
    RESULT_UNEXPECTED = 2,
    RESULT_NOT_IMPLEMENTED = 3,
    RESULT_INIT_FAIL = 4,
    RESULT_NOT_FOUND = 5,
    RESULT_INVALID_ARGUMENT = 6,
    RESULT_OUT_OF_RANGE = 7,
    RESULT_NO_ACCESS = 8,
    RESULT_NULL_ARGUMENT = 9,
};

// template class to pair return value with function result
template<typename... T>
class Result;

template<typename T>
class Result<T>
{
    private:
        T       Val;
        eResult Res;

    public:
        Result(eResult aRes) : Val(), Res(aRes) {};

        Result(eResult aRes, T aRetVal) : Val(aRetVal), Res(aRes) {};

        // Returns stored return value. If the result was not successful then breaks the program
        inline T& Get()
        {
            KR_ASSERT(Res == eResult::RESULT_OK);
            return Val;
        }

        // Checks if result was not successful
        inline bool Failed() const
        {
            return Res != eResult::RESULT_OK;
        }
};

template<>
class Result<void>
{
    private:
        eResult Res;

    public:
        Result(eResult aRes) : Res(aRes) {};

        // Checks if result was not successful
        inline bool Failed() const
        {
            return (Res != eResult::RESULT_OK);
        }
};

template<>
class Result<>
{
    private:
        eResult Res;

    public:
        Result(eResult aRes) : Res(aRes) {};

        // Checks if result was not successful
        inline bool Failed() const
        {
            return (Res != eResult::RESULT_OK);
        }
};

template<typename... T>
bool Failed(const Result<T...>& aRes)
{
    return aRes.Failed();
}

KR_END