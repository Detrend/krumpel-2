#define KR_TESTING
#include <Krumpel.h>

using namespace kr;

bool AllocateSame(const char* FileName)
{
    int OriginalCount = ResourceSystem::Instance().mLoadedMeshes.size();
    cMeshHandle Handle1 = ResourceSystem::Instance().LoadMesh(FileName);
    int BeforeCount = ResourceSystem::Instance().mLoadedMeshes.size();
    cMeshHandle Handle2 = ResourceSystem::Instance().LoadMesh(FileName);
    int AfterCount = ResourceSystem::Instance().mLoadedMeshes.size();

    if (BeforeCount != AfterCount or OriginalCount == BeforeCount)
    {
        return false;
    }
    else
    {
        return true;
    }
}

int main()
{
    // init the resource system
    ResourceSystem::Instance();

    // allocate the same file more times
    KR_ASSERT(AllocateSame("Source/Files/sponza1.krmod") == true);
    KR_ASSERT(AllocateSame("Source/Files/sponza2.krmod") == true);
    KR_ASSERT(AllocateSame("Source/Files/sponza3.krmod") == true);
    KR_ASSERT(AllocateSame("Source/Files/sponza4.krmod") == true);
    KR_ASSERT(AllocateSame("Source/Files/sponza5.krmod") == true);

    return 0;
}