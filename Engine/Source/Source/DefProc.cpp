#include <Windows.h>

#include "Glew.h"
#include "Imgui.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "Window.h"
#include "DefProc.h"
#include "WinProc.h"
#include "Core.h"

KR_BEGIN

LRESULT CALLBACK DefaultWindowProcedure(HWND aWindow, UINT aMessage, WPARAM aWparam, LPARAM aLparam)
{
	cWindow* BaseClass = nullptr;

	// Store pointer to basic window class on window creation, so we can retrieve it every time the procedure is called
	if (aMessage == WM_NCCREATE) {
		LPCREATESTRUCT lpcs = reinterpret_cast<LPCREATESTRUCT>(aLparam);
		BaseClass = static_cast<cWindow*>(lpcs->lpCreateParams);
		cWinProc::StoreWindowPointer(aWindow, BaseClass);
		BaseClass->mHandle = aWindow;
	} else {
		BaseClass = cWinProc::GetWindowPointer(aWindow);
	}

    // This passes the information to ImGui (instead of the app) when it needs it. Without this ImGui will not react to keyboard/mouse input
    if (BaseClass != nullptr and BaseClass->mCore != nullptr and BaseClass->mCore->GetEditor() != nullptr) {
		if (BaseClass->mCore->GetEditor()->WinProcFunction(aWindow, aMessage, aWparam, aLparam)) {
            return true;
		}
    }

	switch (aMessage) {
	case WM_CREATE:
		SetWindowPos(aWindow, 0, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER);
		break;
	case WM_MOUSEMOVE:
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP:
	case WM_MBUTTONDOWN:
	case WM_MBUTTONUP:
	case WM_XBUTTONDOWN:
	case WM_XBUTTONUP:
		[[fallthrough]];
	case WM_INPUT:
		HandleMouseEvent(BaseClass->GetHandle(), aMessage, BaseClass->mMouse, aWparam, aLparam);
		break;
	case WM_KEYDOWN:
	case WM_KEYUP:
		HandleKeyboardEvent(aMessage, BaseClass->mKeyboard, aWparam, aLparam);
		break;
	case WM_SIZE:
		cWinProc::ResizeWindow(BaseClass);
		break;
	case WM_CLOSE:			//	user asks to close window
		DestroyWindow(aWindow);
		break;
	case WM_DESTROY:		//	window is destroyed
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(aWindow, aMessage, aWparam, aLparam);
		break;
	}
	return 0;
}

KR_END