#include <string>
#include <tuple>
#include <utility>
#include <algorithm>

#include "Basics.h"
#include "Generator.h"
#include "OpenGLError.h"
#include "Glew.h"

KR_BEGIN

void cGenerator::GenVertexArray(GLuint& aID)
{
	glGenVertexArrays(1, &aID);
	glBindVertexArray(aID);
}

GLuint cGenerator::GenVertexArray()
{
	GLuint tID;
	glGenVertexArrays(1, &tID);
	glBindVertexArray(tID);
	return tID;
}

void cGenerator::GenVertexBuffer(GLuint& aID, uint32 aSize, void* aData, GLenum aUsage)
{
	glGenBuffers(1, &aID);
	glBindBuffer(GL_ARRAY_BUFFER, aID);
	glBufferData(GL_ARRAY_BUFFER, aSize, aData, aUsage);
}

GLuint cGenerator::GenVertexBuffer(uint32 aSize, void* aData, GLenum aUsage)
{
	GLuint tID;
	glGenBuffers(1, &tID);
	glBindBuffer(GL_ARRAY_BUFFER, tID);
	glBufferData(GL_ARRAY_BUFFER, aSize, aData, aUsage);
	return tID;
}

void cGenerator::SetVertexBuffer(GLuint& aID, uint32 aSize, void* aData, GLenum aUsage)
{
	glBindBuffer(GL_ARRAY_BUFFER, aID);
	glBufferData(GL_ARRAY_BUFFER, aSize, aData, aUsage);
}

void cGenerator::GenIndexBuffer(GLuint& aID, uint32 aCount, void* aData, GLenum aUsage)
{
	glGenBuffers(1, &aID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint) * aCount, aData, aUsage);
}

GLuint cGenerator::GenIndexBuffer(uint32 aCount, void* aData, GLenum aUsage)
{
	GLuint tID;
	glGenBuffers(1, &tID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint) * aCount, aData, aUsage);
	return tID;
}

void cGenerator::SetIndexBuffer(GLuint& aID, uint32 aCount, void* aData, GLenum aUsage)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint) * aCount, aData, aUsage);
}

void cGenerator::SetAttribute(GLuint aID, uint32 aIndex, uint32 aCount, uint32 aStride, uint64 aOffset, GLenum aType, bool aNormalized)
{
	glBindVertexArray(aID);
	glVertexAttribPointer(aIndex, aCount, aType, aNormalized, aStride, (void*)aOffset);
	glEnableVertexAttribArray(aIndex);
}

GLuint cGenerator::GenTexture()
{
	GLuint tID;
	glGenTextures(1, &tID);
	glBindTexture(GL_TEXTURE_2D, tID);
	return tID;
}

void cGenerator::GenTexture(GLuint& aID)
{
	glGenTextures(1, &aID);
	glBindTexture(GL_TEXTURE_2D, aID);
}

void cGenerator::SetTexture(GLuint aID, cTexture& aTexture)
{
	GLenum ChannelsEnum;
	switch (aTexture.GetChannels())
	{
	case 3:
		ChannelsEnum = GL_RGB;
		break;
	case 4:
		ChannelsEnum = GL_RGBA;
		break;
	default:
		KR_BREAKPOINT;
	}

	glBindTexture(GL_TEXTURE_2D, aID);
	glTexImage2D(GL_TEXTURE_2D, 0, ChannelsEnum, aTexture.GetWidth(), aTexture.GetHeight(), 0, ChannelsEnum, GL_UNSIGNED_BYTE, aTexture.GetData());
	glGenerateMipmap(GL_TEXTURE_2D);
}

cMesh cGenerator::GenPlane(std::pair<float, float> aVec1, std::pair<float, float> aVec2, float aHeight, cTexture& aTexture)
{
	cMesh PlaneModel;

	float Vertices[] = { aVec1.first, aHeight, aVec1.second, 0.0, 1.0,
						 aVec1.first, aHeight, aVec2.second, 0.0, 0.0,
						 aVec2.first, aHeight, aVec2.second, 1.0, 0.0,
						 aVec2.first, aHeight, aVec1.second, 1.0, 1.0
	};

	uint Indices[] = { 0, 1, 2,
					   0, 2, 3
	};

	PlaneModel.SetVertexData(Vertices, sizeof(Vertices));
	PlaneModel.SetIndexData(Indices, sizeof(Indices));

	return PlaneModel;
}

cMesh cGenerator::GenCube(std::tuple<float, float, float> aVec1,
						   std::tuple<float, float, float> aVec2)
{
	auto x1 = std::max(std::get<0>(aVec1), std::get<0>(aVec2));
	auto y1 = std::max(std::get<1>(aVec1), std::get<1>(aVec2));
	auto z1 = std::max(std::get<2>(aVec1), std::get<2>(aVec2));

	auto x2 = std::min(std::get<0>(aVec1), std::get<0>(aVec2));
	auto y2 = std::min(std::get<1>(aVec1), std::get<1>(aVec2));
	auto z2 = std::min(std::get<2>(aVec1), std::get<2>(aVec2));
	
	cMesh CubeModel;

	float Vertices[] = {
		// bottom
		x2, y2, z2, 0.0f, 0.0f,		//	0
		x1, y2, z2, 1.0f, 0.0f,
		x2, y2, z1, 0.0f, 1.0f,
		x1, y2, z1, 1.0f, 1.0f,

		// top
		x2, y1, z2, 0.0f, 1.0f,		//	4
		x2, y1, z1, 0.0f, 0.0f,
		x1, y1, z2, 1.0f, 1.0f,
		x1, y1, z1, 1.0f, 0.0f,

		// south
		x1, y1, z1, 1.0f, 1.0f,		//	8
		x2, y1, z1, 0.0f, 1.0f,
		x2, y2, z1, 0.0f, 0.0f,
		x1, y2, z1, 1.0f, 0.0f,

		// north
		x1, y1, z2, 0.0f, 1.0f,		//	12
		x2, y1, z2, 1.0f, 1.0f,
		x2, y2, z2, 1.0f, 0.0f,
		x1, y2, z2, 0.0f, 0.0f,

		// west
		x2, y1, z1, 1.0f, 1.0f,		//	16
		x2, y1, z2, 0.0f, 1.0f,
		x2, y2, z2, 0.0f, 0.0f,
		x2, y2, z1, 1.0f, 0.0f,

		// east
		x1, y1, z1, 0.0f, 1.0f,		//	20
		x1, y1, z2, 1.0f, 1.0f,
		x1, y2, z2, 1.0f, 0.0f,
		x1, y2, z1, 0.0f, 0.0f
	};

	uint Indices[] = {0, 1, 2,
					  1, 2, 3,
					  
					  4, 5, 6,
					  6, 5, 7,
					  
					  8, 9, 10,
					  8, 10, 11, 

					  12, 14, 13,
					  12, 15, 14,
					  
					  16, 17, 18,
					  18, 19, 16,
					  
					  21, 20, 22,
					  23, 22, 20 };

	CubeModel.SetVertexData(Vertices, sizeof(Vertices));
	CubeModel.SetIndexData(Indices, sizeof(Indices));

	return CubeModel;
}

cMesh cGenerator::GenScreen()
{
	cMesh Model;

	float Vertices[] = { -0.5f,  0.5f, 0.0f, 0.0,  1.0,
						 -0.5f, -0.5f, 0.0f, 0.0f, 0.0f,
						  0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
						  0.5f,  0.5f, 0.0f, 1.0f, 1.0f
	};

	uint Indices[] = { 0, 1, 2,
					   0, 2, 3
	};

	Model.SetVertexData(Vertices, sizeof(Vertices));
	Model.SetIndexData(Indices, sizeof(Indices));

	return Model;
}

KR_END