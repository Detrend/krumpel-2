#pragma once

#include "Basics.h"

KR_BEGIN

constexpr bool IsBigEndian()
{
    union {
        uint32 u32;
        uint8  u8[4];
    } u = { 0x01020304 };
    return (u.u8[0] == 1);
}

KR_END