#pragma once

#include "Macros.h"

KR_BEGIN

class iNullReference
{
public:
    virtual bool IsNull() const = 0;

    virtual ~iNullReference() = 0;
};

KR_END