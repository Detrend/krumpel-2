#pragma once

#include <Windows.h>
#include <glm/glm.hpp>
#include <string>
#include <vector>
#include <tuple>
#include <filesystem>

#include "Glew.h"
#include "Basics.h"
#include "Shader.h"
#include "GlHandle.h"

KR_BEGIN

// Supported shader types
enum eShaderType {
    SHADER_VERTEX = 0,
    SHADER_FRAGMENT = 1,
    SHADER_GEOMETRY = 2
};

// Represents a certain type of a shader
class cShaderComponent : public iGlHandle
{
public:
    bool		mCompiled;
    eShaderType mShaderType;
    GLuint		mID;
    std::string mPath;

    // Creates this shader component
    void CreateShaderComponent();
    // Destroys this component
    void DestroyShaderComponent();
    // Returns true if this component was already created
    bool IsAlive();

public:
    cShaderComponent(eShaderType Type) : mCompiled(false), mShaderType(Type), mID(0) {}
    ~cShaderComponent();
    // Returns an OpenGL handle to shader object
    GLuint GetHandle() const;
    // Compiles a shader code from the file
    Result<> CompileFromFile(const std::filesystem::path& Path);
    // Returns true if shader was succesfully compiled
    bool IsCompiled() const;
};

enum eShaderRequirement {
    REQUIRE_UNIFORM_MAT4_PROJECTION         = 0,			// Projection matrix
    REQUIRE_UNIFORM_MAT4_WORLD		        = 1,			// World to camera matrix
    REQUIRE_UNIFORM_MAT4_OBJECT		        = 2,			// Object to world matrix
    REQUIRE_UNIFORM_SAMPLER2D_MAIN_TEXTURE  = 3,		    // Object texture
};

struct sRequirementSlotPair
{
    eShaderRequirement Requirement;
    uint Slot;
};

inline std::vector<std::tuple<std::string, eShaderRequirement>> PossibleShaderRequirements;

// Represents a GPU program
class cShader : public iGlHandle, public iNullReference
{
private:
    GLuint  mID;
    bool    mLinked;
    // Creates shader program
    void CreateProgram();
    // Destroys the program
    void DestroyProgram();
    // Checks if shader program was already created
    bool IsAlive();
    
    std::list<sRequirementSlotPair> mRequirements;

public:
    cShaderComponent Vertex;
    cShaderComponent Geometry;
    cShaderComponent Fragment;

public:
    cShader();
    ~cShader();

    // Returns its OpenGL handle to GPU program object
    GLuint GetHandle() const;
    // Binds the GPU program
    void Bind() const;
    // Returns true if this object is just a null reference
    virtual bool IsNull() const override;
    // Links the GPU program if not yet linked
    Result<> Link();
    // Links shader from file. Structure of the file is as follows:
    // #include_fragment [Fragment path]
    // #include_vertex [Vertex path]
    // (optional) #include_geometry [Geometry path]
    Result<> LinkFromFile(const std::filesystem::path& Path);
    // Returns non mutable list of requirements
    const std::list<sRequirementSlotPair>& GetRequirements() const;
    // Returns true if this GPU program was successfully linked
    bool IsLinked() const;
};

inline const cShader NullShader;

KR_END