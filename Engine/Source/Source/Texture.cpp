#include <utility>
#include <iostream>

#include "Texture.h"
#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>


KR_BEGIN

cTexture::cTexture()
	: mWidth(0), mHeight(0), mChannels(0), mInGpu(false), mHandle(0), mData(nullptr, FreeTextureData)
{}

cTexture::cTexture(cTexture&& aOther)
	: mWidth(aOther.mWidth),
	  mHeight(aOther.mHeight),
	  mChannels(aOther.mChannels),
	  mData(std::move(aOther.mData)),
	  mHandle(aOther.mHandle)
{
	aOther.mWidth = 0;
	aOther.mHeight = 0;
	aOther.mChannels = 0;
	aOther.mHandle = 0;
}

cTexture::~cTexture()
{
	FreeData();
}

cTexture& cTexture::operator=(cTexture&& aOther)
{
	if (this != &aOther) {
		FreeData();
		mWidth = aOther.mWidth;
		aOther.mWidth = 0;
		mHeight = aOther.mHeight;
		aOther.mHeight = 0;
		mChannels = aOther.mChannels;
		aOther.mChannels = 0;
		mData = std::move(aOther.mData);
		mHandle = aOther.mHandle;
		aOther.mHandle = 0;
	}

	return *this;
}

void cTexture::FreeData()
{
	if (mData) {
		mData.reset();
	}
}

void cTexture::UploadToGpu()
{
	if (!InGpu() and mData != nullptr)
	{
		glGenTextures(1, &mHandle);
		glBindTexture(GL_TEXTURE_2D, mHandle);
		GLint TextureFormat = mChannels == 3 ? GL_RGB : GL_RGBA;
		glTexImage2D(GL_TEXTURE_2D, 0, TextureFormat, mWidth, mHeight, 0, TextureFormat, GL_UNSIGNED_BYTE, mData.get());
		glGenerateMipmap(GL_TEXTURE_2D);

		mInGpu = true;
	}
}

void cTexture::FreeFromGpu()
{
	if (glIsTexture(mHandle))
	{
		glDeleteTextures(1, &mHandle);
		mInGpu = false;
	}
}

bool cTexture::InGpu() const
{
	return mInGpu;
}

GLuint cTexture::GetHandle() const
{
	return mHandle;
}

uint cTexture::GetWidth() const
{
	return mWidth;
}

uint cTexture::GetHeight() const
{
	return mHeight;
}

uint cTexture::GetChannels() const
{
	return mChannels;
}

byte* cTexture::GetData() const
{
	return mData.get();
}

bool cTexture::IsNull() const
{
	return (this == &NullTexture);
}

void cTexture::FreeTextureData(byte* aData)
{
	if (aData != nullptr)
	{
        stbi_image_free(aData);
	}
}

eResult cTexture::LoadFromFile(const char* aFilename)
{
	int Width, Height, Channels;
	stbi_set_flip_vertically_on_load(true);
	byte* TextureData = stbi_load(aFilename, &Width, &Height, &Channels, 0);

	FreeData();
	if (TextureData != nullptr)
	{
		mWidth = Width;
		mHeight = Height;
		mChannels = Channels;
		mData = std::unique_ptr<byte, std::function<void(byte*)>>(TextureData, FreeTextureData);

		return eResult::RESULT_OK;
	}
	else
	{
		// If we failed to load the data
		return eResult::RESULT_FAIL;
	}
}

KR_END