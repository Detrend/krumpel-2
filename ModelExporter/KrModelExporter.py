# uses this: https://pypi.org/project/PyWavefront
# C:\Users\matus\AppData\Local\Packages\PythonSoftwareFoundation.Python.3.9_qbz5n2kfra8p0\LocalCache\local-packages\Python39\site-packages\pywavefront

import pywavefront as pw

scene = pw.Wavefront("C:\Code\Krumpel\krumpel-2\ModelExporter\models\cottage_obj.obj", collect_faces=True, create_materials=True)

print("tex indices:", len(scene.parser.tex_coords))
print("vertices:", len(scene.vertices))
for name, mesh in scene.meshes.items():
    print("faces:", len(mesh.faces))

# with open("gauc_log.txt", "w") as log:
#     print("VERTICES:", file=log)
#     print(scene.vertices, file=log)

# print(scene.vertices)
# for mesh in scene.mesh_list:
#     print()
#     print("mesh:", mesh.name)
#     print(mesh.faces)
#     print()
    
    # print("Vertices:", material.vertices)
