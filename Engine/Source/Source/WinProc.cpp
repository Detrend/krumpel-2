#pragma once

#include <Windows.h>

#include "Glew.h"
#include "Basics.h"
#include "WinProc.h"
#include "Window.h"

KR_BEGIN

void cWinProc::StoreWindowPointer(HWND aHandle, cWindow* aPointer)
{
	SetWindowLongPtr(aHandle, GWLP_USERDATA, (LONG_PTR)aPointer);
}

cWindow* cWinProc::GetWindowPointer(HWND aHandle)
{
	return (cWindow*)GetWindowLongPtr(aHandle, GWLP_USERDATA);
}

void cWinProc::ResizeWindow(cWindow* aWindow)
{
	if (aWindow == nullptr) {
		return;
	}

	RECT win_size;
	GetClientRect(aWindow->GetHandle(), &win_size);
	uint Width = win_size.right;
	uint Height = win_size.bottom;

	aWindow->mWidth = Width;
	aWindow->mHeight = Height;

	wglMakeCurrent(aWindow->GetDeviceContext(), aWindow->GetRenderingContext());
	glViewport(0, 0, Width, Height);
}

KR_END