//
//	DEPRECATED!!!
//
#include "OpenGLError.h"
#include <GLEW/glew.h>

KR_BEGIN

void cOpenGLError::ClearErrorQueue()
{
	GLenum error = GL_NO_ERROR;
	do {
		error = glGetError();
	} while (error != GL_NO_ERROR);
}

void cOpenGLError::CheckError()
{
	GLenum error = glGetError();
	switch (error) {
	case GL_NO_ERROR: break;
	case GL_INVALID_ENUM:
		PrintGlError(GL_INVALID_ENUM);
		break;
	case GL_INVALID_VALUE:
		PrintGlError(GL_INVALID_VALUE);
		break;
	case GL_INVALID_OPERATION:
		PrintGlError(GL_INVALID_OPERATION);
		break;
	case GL_INVALID_FRAMEBUFFER_OPERATION:
		PrintGlError(GL_INVALID_FRAMEBUFFER_OPERATION);
		break;
	case GL_OUT_OF_MEMORY:
		PrintGlError(GL_OUT_OF_MEMORY);
		break;
	case GL_STACK_UNDERFLOW:
		PrintGlError(GL_STACK_UNDERFLOW);
		break;
	case GL_STACK_OVERFLOW:
		PrintGlError(GL_STACK_OVERFLOW);
		break;
	default:
		PrintGlError(OTHER);
		break;
	}
}

KR_END