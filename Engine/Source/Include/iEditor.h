#pragma once

#include "Basics.h"
#include <Windows.h>

KR_BEGIN

class cCore;

class iEditor
{
    public:
        virtual Result<> Init(cCore* CoreAddress) = 0;

        virtual Result<> Terminate() = 0;

        virtual void Update() = 0;

        virtual void Render() = 0;

        virtual bool BlockMouse() = 0;

        virtual bool BlockKeyboard() = 0;

        virtual bool WinProcFunction(HWND, UINT, WPARAM, LPARAM) = 0;
};

KR_END
