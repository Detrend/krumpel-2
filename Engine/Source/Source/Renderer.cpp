#include <glm/gtc/type_ptr.hpp>
#include <stb/stb_image.h>

#include <list>

#include "ResourceSystem.h"
#include "Basics.h"
#include "Renderer.h"
#include "Shader.h"
#include "Mesh.h"
#include "Shader.h"
#include "Camera.h"
#include "Scene.h"
#include "Window.h"
#include "OpenGLError.h"
#include "Generator.h"

KR_BEGIN

cRenderer3D::cRenderer3D() : mRenderingMode(KR_RENDER_MODE_NORMAL),
                             mFaceCullingMode(KR_FACE_CULLING_BACK),
                             mTextureFilteringMode(KR_TEXTURE_FILTERING_NEAREST)
{
#ifdef DEVTOOLS
    auto GizmoShaderSetup = mGizmoShader.LinkFromFile(GizmoShaderPath);
    if (Failed(GizmoShaderSetup)) {
        Logger.Error("Failed to link Gizmo shader", "Renderer init");
    }
#endif
}

void cRenderer3D::SetRenderingMode(eRenderMode aMode)
{
    mRenderingMode = aMode;
}

void cRenderer3D::SetFaceCulling(eFaceCulling aMode)
{
    mFaceCullingMode = aMode;
}

void cRenderer3D::Render(GLuint aVertexArray,
    uint aElementCount,
    GLuint aShaderProgram,
    GLint aUniform1ID,
    const glm::mat4& aMatrix1,
    GLint aUniform2ID,
    const glm::mat4& aMatrix2,
    GLint aUniform3ID,
    const glm::mat4& aMatrix3)
{
    glBindVertexArray(aVertexArray);
    glUseProgram(aShaderProgram);

    glUniformMatrix4fv(aUniform1ID, 1, false, glm::value_ptr(aMatrix1));
    glUniformMatrix4fv(aUniform2ID, 1, false, glm::value_ptr(aMatrix2));
    glUniformMatrix4fv(aUniform3ID, 1, false, glm::value_ptr(aMatrix3));

    switch (mRenderingMode) {
    case KR_RENDER_MODE_NORMAL:
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        break;
    case KR_RENDER_MODE_WIREFRAME:
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        break;
    case KR_RENDER_MODE_POINTS:
        glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
        break;
    }

    switch (mFaceCullingMode) {
    case KR_FACE_CULLING_NONE:
        glDisable(GL_CULL_FACE);	
        break;
    case KR_FACE_CULLING_BACK:
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        break;
    case KR_FACE_CULLING_FRONT:
        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);
        break;
    }

    glDrawElements(GL_TRIANGLES, aElementCount, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
}

void cRenderer3D::RenderObject(const cObject& aActor, const cCamera& aCamera, const glm::mat4& aTransformation)
{
    glm::mat4 WorldToCamera = aCamera.GetWorldToView();
    glm::mat4 Frustum = aCamera.GetProjection();
    glm::mat4 ObjectToWorld = aTransformation * aActor.GetTransform();

    cShaderHandle ShaderHandle = aActor.GetMaterial();
    auto&& Shader = ResourceSystem::Instance().GetShader(ShaderHandle);
    cTextureHandle TextureHandle = aActor.GetTexture();
    auto&& Texture = ResourceSystem::Instance().GetTexture(TextureHandle); 
    cMeshHandle MeshHandle = aActor.GetMesh();
    auto&& Mesh = ResourceSystem::Instance().GetMesh(MeshHandle);

    // TODO: do this only on the beginning of the frame. We do not need to rebind the shader every time
    if (!Texture.IsNull() and !Mesh.IsNull() and !Shader.IsNull() and Texture.InGpu() and Mesh.InGpu() and Shader.IsLinked())
    {
        Shader.Bind();
        glBindVertexArray(Mesh.GetVertexArrayHandle());

        for (auto&& RequirementPair : Shader.GetRequirements())
        {
            // Bind shader requirements
            switch (RequirementPair.Requirement)
            {
                case REQUIRE_UNIFORM_MAT4_PROJECTION:
                    glUniformMatrix4fv(RequirementPair.Slot, 1, false, glm::value_ptr(Frustum));
                break;
                case REQUIRE_UNIFORM_MAT4_WORLD:
                    glUniformMatrix4fv(RequirementPair.Slot, 1, false, glm::value_ptr(WorldToCamera));
                break;
                case REQUIRE_UNIFORM_MAT4_OBJECT:
                    glUniformMatrix4fv(RequirementPair.Slot, 1, false, glm::value_ptr(ObjectToWorld));
                break;
                case REQUIRE_UNIFORM_SAMPLER2D_MAIN_TEXTURE:
                    glUniform1i(RequirementPair.Slot, 0);				// bind texture slot 0 to uniform location 3
                    glActiveTexture(GL_TEXTURE0);
                    glBindTexture(GL_TEXTURE_2D, Texture.GetHandle());
                break;
            }
        }

        // Render
        glDrawElements(GL_TRIANGLES, Mesh.GetIndexCount(), GL_UNSIGNED_INT, 0);
    }

    // Render recursively all its children
    for (auto&& Child : aActor.GetChildren())
    {
        glm::mat4 NewTransform = Child->GetTransform() * ObjectToWorld; 
        RenderObject(*Child.get(), aCamera, NewTransform);
    }
}

void cRenderer3D::RenderLines(const cCamera& aCamera, 
                                   const glm::mat4& aTranslation,
                                   glm::vec3 aColor, 
                                   const std::vector<glm::vec3>& aLines,
                                   bool aDoDepthTest,
                                   int aLineWidth, bool aScale)
{
    // Exit if the gizmo shader is not working
    if (!mGizmoShader.IsLinked())
        return;

    bool PreviousDepthTestValue = glIsEnabled(GL_DEPTH_TEST);
    if (aDoDepthTest) {
        glEnable(GL_DEPTH_TEST);
    } else {
        glDisable(GL_DEPTH_TEST);
    }

    // Set line width
    // TODO: this is deprecated in newer versions of OpenGL
    glLineWidth(static_cast<float>(aLineWidth));

    const glm::mat4& WorldToCamera = aCamera.GetWorldToView();
    const glm::mat4& Frustum = aCamera.GetProjection();
    glm::mat4 Translation = aTranslation;
    
    // if scale, then do something
    if (aScale) {
        glm::vec4 Zero = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
        glm::vec3 Location = glm::vec3(Translation * Zero);
        glm::vec3 Up = glm::vec3(0.0f, 1.0f, 0.0f);
        glm::mat4 LookAtDirectly = glm::lookAt(aCamera.GetPosition(), Location, Up);
        glm::vec4 ScreenPosition = Frustum * LookAtDirectly * Translation * Zero;
        float ScaleFactor = ScreenPosition.w;
        Translation = glm::scale(Translation, glm::vec3(ScaleFactor));
    }

    // create vbo
    GLuint Vao;
    glGenVertexArrays(1, &Vao);
    glBindVertexArray(Vao);

    // Create temporary vertex buffer. This is inefficient as hell, but as this is used only in
    // developer mode we can go on with it
    GLuint vbo = cGenerator::GenVertexBuffer(static_cast<uint32>(aLines.size() * 3 * sizeof(float)), (void*)&(aLines[0].x));
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    // vertex attribute pointers
    glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, nullptr);
    glEnableVertexAttribArray(0);

    // bind the shader
    mGizmoShader.Bind();

    // fill the uniforms
    glUniformMatrix4fv(0, 1, false, glm::value_ptr(Frustum));           // projection
    glUniformMatrix4fv(1, 1, false, glm::value_ptr(WorldToCamera));     // camera rotation
    glUniformMatrix4fv(2, 1, false, glm::value_ptr(Translation));      // lines translation
    glUniform4f(3, aColor.r, aColor.g, aColor.b, 1.0f);                 // lines color
    
    // render
    glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(aLines.size()));

    // destroy temporary vao
    glBindVertexArray(0);
    glDeleteVertexArrays(1, &Vao);

    // destroy temporary vbo
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, &vbo);

    // Restore previous depth test value
    if (PreviousDepthTestValue) {
        glEnable(GL_DEPTH_TEST);
    } else {
        glDisable(GL_DEPTH_TEST);
    }
}

void cRenderer3D::Render(const cScene& Scene)
{
    const auto& Objects = Scene.GetObjects();
    const auto& Camera = Scene.GetCamera();
    const glm::mat4 Identity(1.0f);	

    for (auto&& Object : Objects) {
        RenderObject(*Object.get(), Camera, Identity);		
    }
}

KR_END