#pragma once

#include <Windows.h>
#include <bitset>

#include "Basics.h"

KR_BEGIN

LRESULT CALLBACK DefaultWindowProcedure(HWND, UINT, WPARAM, LPARAM);

class cWindow;

enum class MouseButton
{
    Left = 0,       // Left mouse button
    Right = 1,      // Right mouse button
    Middle = 2,     // Middle mouse button
    Extra1 = 3,     //
    Extra2 = 4,     // 
    Count = 5       // Number of mouse buttons
};

class cMouse
{
    private:
        int64			mXCoord;
        int64			mYCoord;
        int64           mXChange;
        int64           mYChange;
        std::bitset<(int)MouseButton::Count> mCheckedButtons;
        std::bitset<(int)MouseButton::Count> mPressedButtons;
        std::bitset<(int)MouseButton::Count> mReleasedButtons;

    protected:
        // Setters for Window procedure
        void SetChecked(MouseButton Button, bool State);
        void SetPressed(MouseButton Button, bool State);
        void SetReleased(MouseButton Button, bool State);
        void SetCoord(int64 Xcoord, int64 Ycoord);
        void SetChange(int64 XCoord, int64 YCoord);

    public:
        // Returns true if button is pressed
        bool IsPressed(MouseButton Button) const;

        bool IsChecked(MouseButton Button) const;

        bool IsReleased(MouseButton Button) const;

        // Returns mouse X relative to window
        int64 GetMouseX() const;

        // Returns mouse Y relative to window
        int64 GetMouseY() const;

        // Returns raw mouse change in X direction
        int64 GetChangeX() const;

        // Returns raw mouse change in Y direction
        int64 GetChangeY() const;

        // Sets position of cursor relative to window
        void SetCursorPosition(const cWindow& Window, int64 x, int64 y);

        // Clears data on input and output. Has to be called at every update end
        void Clear();

friend LRESULT CALLBACK DefaultWindowProcedure(HWND, UINT, WPARAM, LPARAM);
friend void HandleMouseEvent(HWND, UINT, cMouse*, WPARAM, LPARAM);
};

void HandleMouseEvent(HWND Window, UINT Msg, cMouse* Mouse, WPARAM Wparam, LPARAM Lparam);

KR_END