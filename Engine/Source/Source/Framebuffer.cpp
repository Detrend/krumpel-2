#include "Framebuffer.h"
#include "Logger.h"
#include <cstdlib>


KR_BEGIN

Result<> cFramebuffer::Create(uint32 aWidth, uint32 aHeight, uint32 aMSAA, uint32 aCount)
{
    if (aCount < 1 or aCount >= FRAMEBUFFER_TEXTURE_COMPONENTS) {
        Logger.Warning("Framebuffer creation failed due to invalid argument", "Framebuffer");
        return {RESULT_INVALID_ARGUMENT};
    }

    if (glIsFramebuffer(mFramebufferHandle)) {
        glDeleteFramebuffers(1, &mFramebufferHandle);
    }

    glGenFramebuffers(1, &mFramebufferHandle);
    glBindFramebuffer(GL_FRAMEBUFFER, mFramebufferHandle);
    KR_ASSERT(glIsFramebuffer(mFramebufferHandle));

    for (int i = 0; i < FRAMEBUFFER_TEXTURE_COMPONENTS; i++) {
        if (glIsTexture(mTextureHandles[i])) {
            glDeleteTextures(1, &(mTextureHandles[i]));
        }
        mTextureHandles[i] = 0;
    }

    if (glIsTexture(mDepthBufferHandle)) {
        glDeleteTextures(1, &mDepthBufferHandle);
    }

    glGenTextures(1, &mDepthBufferHandle);
    glGenTextures(aCount, mTextureHandles);

    if (aMSAA > 1) {
        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, mDepthBufferHandle);
        glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, aMSAA, GL_DEPTH_COMPONENT32F, aWidth, aHeight, GL_TRUE);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D_MULTISAMPLE, mDepthBufferHandle, 0);
    } else {
        glBindTexture(GL_TEXTURE_2D, mDepthBufferHandle);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, aWidth, aHeight, 0, GL_DEPTH_COMPONENT, GL_BYTE, nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, mDepthBufferHandle, 0);
    }

    // generate color attachments
    for (uint32 i = 0; i < aCount; i++) {
        if (aMSAA > 1) {
            glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, mTextureHandles[i]);
            glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, aMSAA, GL_RGBA8, aWidth, aHeight, GL_TRUE);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D_MULTISAMPLE, mTextureHandles[i], 0);
        } else {
            glBindTexture(GL_TEXTURE_2D, mTextureHandles[i]);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, aWidth, aHeight, 0, GL_RGBA, GL_BYTE, nullptr);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, mTextureHandles[i], 0);
        }
    }

    constexpr GLenum DrawBuffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1,
                            GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3,
                            GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5,
                            GL_COLOR_ATTACHMENT6, GL_COLOR_ATTACHMENT7};
    glDrawBuffers(aCount, DrawBuffers);

    GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (Status != GL_FRAMEBUFFER_COMPLETE) {
        Logger.Warning("Failed to create framebuffer", "Framebuffer");
        return {RESULT_FAIL};
    }

    mTexturesCount = aCount;
    mValid = true;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    Logger.Notify("Created a framebuffer", "Framebuffer");
    return {RESULT_OK};
}

Result<> cFramebuffer::Free()
{
    for (std::size_t i = 0; i < FRAMEBUFFER_TEXTURE_COMPONENTS; i++) {
        if (glIsTexture(mTextureHandles[i])) {
            glDeleteTextures(1, &(mTextureHandles[i]));
        }
        mTextureHandles[i] = 0;
    }

    if (glIsFramebuffer(mFramebufferHandle)) {
        glDeleteFramebuffers(1, &mFramebufferHandle);
    }

    mValid = false;
    Logger.Notify("Freed a framebuffer from memory", "Framebuffer");
    return {RESULT_OK};
}

cFramebuffer::~cFramebuffer()
{
    Free();
}

Result<> cFramebuffer::Bind()
{
    if (IsValid()) {
        glBindFramebuffer(GL_FRAMEBUFFER, mFramebufferHandle);
        return {RESULT_OK};
    } else {
        return {RESULT_FAIL};
    }
}

KR_END