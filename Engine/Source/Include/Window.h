#pragma once

#include <Windows.h>
#include <windowsx.h>
#include <WinUser.h>
#include <string>

#include "Basics.h"
#include "DefProc.h"

KR_BEGIN

class cWinProc;
class cKeyboard;
class cMouse;
class cCore;

constexpr const wchar_t* DEFAULT_WINDOW_CLASS_NAME = L"Krumpel Default Window";

class cWindow
{
    friend LRESULT CALLBACK DefaultWindowProcedure(HWND, UINT, WPARAM, LPARAM);
    friend class cWinProc;
    private:
        cCore*						mCore;
        HWND						mHandle;
        tWindowProcedure			mWinProc;
        
        HDC							mDeviceContext;
        HGLRC						mRenderingContext;
        
        cKeyboard*					mKeyboard;
        cMouse*						mMouse;
        UINT						mWidth;
        UINT						mHeight;
        BOOL						mFullscreen;
        BOOL						mClosed = FALSE;
        MSG							mMessage;

        bool						mHasRenderingContext;
        bool						mIsInitialized;

    private:
        Result<> InitOpenGl();

        Result<> SetupWin32Class(HINSTANCE ProgramHandle);

        Result<> TerminateOpenGl();

        Result<> SetFullscreenState(bool State);

        static void InitOpenGlPixelFormatDescritor(PIXELFORMATDESCRIPTOR& Pfd);

        int	PeekMsg();

        int	ProcessMsg();

    public:
        cWindow();

        ~cWindow();
        // Creates a window with desired name, size and fullscreen state.
        // Core must be pointer to engine core.
        Result<> Init(cCore* Core, const char* WindowName, int WindowWidth, int WindowHeight, bool Fullscreen);
        // Creates a window with desired name, size and fullscreen state.
        // Core must be pointer to engine core.
        Result<> Init(cCore* Core, const wchar_t* WindowName, int WindowWidth, int WindowHeight, bool Fullscreen);
        // Returns WinApi handle to this window
        HWND GetHandle() const;
        // Returns WinApi handle to graphics device context
        HDC GetDeviceContext() const;
        // Returns WinApi handle to rendering context
        HGLRC GetRenderingContext() const;
        // Closes the window. Subsequent calls to 'IsClosed' will return true
        Result<> Close();
        // Gives window a pointer to keyboard.
        // Subsequent messages to this window will update this keyboard
        Result<> SetKeyboard(cKeyboard* Keyboard);
        // Gives window a pointer to mouse.
        // Subsequent messages to this window will update this mouse
        Result<> SetMouse(cMouse* Mouse);
        // Sets the window to fullscreen state
        Result<> SetFullscreen();
        // Sets the window state to windowed (opposite of fullscreen)
        Result<> SetWindowed();
        // Sets caption of the window
        Result<> SetCaption(const wchar_t* Text);
        // Sets caption of the window
        Result<> SetCaption(const char* Text);
        // Returns true if the window is set to fullscreen mode
        bool IsFullscreen();
        // Pumps windows messages this window should receive. This updates mouse and keyboard
        void ProcessMessages();
        // Checks wheather window was already closed
        int IsClosed();
        // Returns width of the client area of the window in pixels
        int GetWidth() const;
        // Returns height of the client area of the window in pixels
        int GetHeight() const;
        // Makes this window's render context current
        void BindContext();
        // Turns on default windows vsync
        void SetVsync(bool On);
        // Checks wheather default window vsync is on
        bool HasWinVsync();
        // Swaps front and back buffers of the window
        void Swap();
};

KR_END