#pragma once

#include <Windows.h>
#include <fstream>
#include <iostream>
#include <string>
#include <ostream>

#include "Basics.h"
#include "Glew.h"
#include "Printable.h"
#include "LoggerUtil.h"

KR_BEGIN

/* Additional objects to format output */
inline cPrintableCharacter Endl("\n");
inline cPrintableCharacter Tab("\t");
inline cPrintableColor ColorRed(12);
inline cPrintableColor ColorGreen(10);
inline cPrintableColor ColorWhite(7);
inline cPrintableColor ColorGray(8);
inline cPrintableColor ColorBlue(9);
inline cPrintableColor ColorYellow(14);
inline cPrintableColor ColorPink(13);

enum LoggerSeverityLevel
{
    ReportNothing = 0,          // report nothing
    ReportOnlyCritical = 1,     // report only errors
    ReportWarnings = 2,         // report warnings and errors
    ReportEverything = 3        // report everything
};

/* Simple logging class. Can be bound to console or file */
class cLog
{
private:
    std::ofstream	            mFile;
    LoggerSeverityLevel	        mEngineSeverity;
    LoggerSeverityLevel         mOpenGlSeverity;
    bool                        mFileOpenedByMe;

    // callback for opengl errors
    static void GLAPIENTRY GlDebugCallback(GLenum source,
        GLenum type,
        GLuint id,
        GLenum severity,
        GLsizei length,
        const GLchar* message,
        const void* userParam);
public:
    cLog();

    /* Opens/creates file. If opened existing file, 
    then content of the file is rewritten */
    cLog(const std::string& FileName);

    /* Opens/creates file. If opened existing file,
    then content of the file is rewritten */
    cLog(const char* FileName);

    cLog(std::ostream& Output);

    // Opens or creates new file
    Result<void> Open(const std::string& FileName);

    // Opens or creates new file
    Result<void> Open(const char* FileName);

    // Writes to provided output
    Result<void> Open(std::ostream& Output);

    cLog& operator<<(const char* String);

    cLog& operator<<(const cPrintable& Printable);

    cLog& Print(const char* String);

    cLog& Print(const cPrintable& Printable);

    cLog& PrintWinError(DWORD Err);

    void SetSeverityLevel(LoggerSeverityLevel Level);

    void SetOpenGlSeverityLevel(LoggerSeverityLevel Level);

    LoggerSeverityLevel GetSeverityLevel();

    LoggerSeverityLevel GetOpenGlSeverityLevel();

    void Notify(const char* Message, const char* Source = nullptr);

    void NotifyOpenGl(const char* Message, const char* Source = nullptr);

    void Warning(const char* Message, const char* Source = nullptr);

    void WarningOpenGl(const char* Message, const char* Source = nullptr);

    void Error(const char* Message, const char* Source = nullptr);

    void ErrorOpenGl(const char* Message, const char* Source = nullptr);

    // Prints WinAPI error
    void WinError(DWORD Err, const char* Source = nullptr);

    eResult EnableOpenGlDebugLog();

    /* Flushes the file and closes the file stream */
    ~cLog();
};

inline cLog Logger(std::cout);

KR_END