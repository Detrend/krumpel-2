#version 330 core
#extension GL_ARB_explicit_uniform_location : require

layout(location = 0) in vec3 aPos;

layout(location = 0) uniform mat4 uProjection;
layout(location = 1) uniform mat4 uTranslateView;
layout(location = 2) uniform mat4 uTranslateModel;

void main(void)
{
	gl_Position = uProjection * uTranslateView * uTranslateModel * vec4(aPos, 1.0);
}