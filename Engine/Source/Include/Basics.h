// This includes all headers that every engine source file might need

#pragma once
#include "Macros.h"
#include "Types.h"
#include "Globals.h"
#include "KrError.h"
#include "NullReference.h"