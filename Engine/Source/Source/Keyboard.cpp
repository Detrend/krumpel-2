#include "Keyboard.h"
#include "Basics.h"
#include <iostream>

KR_BEGIN

cKeyboard::cKeyboard()
{
	mCheckedKeys.reset();
}

void cKeyboard::SetChecked(KeyCode key, bool state)
{
	mCheckedKeys[key % 256] = state;
}

void cKeyboard::SetPressed(KeyCode key, bool state)
{
	mPressedKeys[key % 256] = state;
}

void cKeyboard::SetReleased(KeyCode key, bool state)
{
	mReleasedKeys[key % 256] = state;
}

bool cKeyboard::IsChecked(KeyCode key)
{
	return mCheckedKeys[key % 256];
}

bool cKeyboard::IsPressed(KeyCode key)
{
	bool ret = mPressedKeys[key % KEYBOARD_KEYS_COUNT];
	mPressedKeys[key % KEYBOARD_KEYS_COUNT] = false;
	return ret;
}

bool cKeyboard::IsReleased(KeyCode key)
{
	bool ret = mReleasedKeys[key % KEYBOARD_KEYS_COUNT];
	mReleasedKeys[key % KEYBOARD_KEYS_COUNT] = false;
	return ret;
}

void cKeyboard::Clear()
{
	mPressedKeys.reset();
	mReleasedKeys.reset();
}

void HandleKeyboardEvent(UINT msg, cKeyboard* keyboard, WPARAM wparam, LPARAM lparam) {
	if (keyboard != nullptr) {
		switch (msg) {
		case WM_KEYDOWN:
			if (!keyboard->IsChecked(static_cast<unsigned char>(wparam))) {
				keyboard->SetChecked(static_cast<unsigned char>(wparam), true);
				keyboard->SetPressed(static_cast<unsigned char>(wparam), true);
			}
			break;
		case WM_KEYUP:
			if (keyboard->IsChecked(static_cast<unsigned char>(wparam))) {
				keyboard->SetChecked(static_cast<unsigned char>(wparam), false);
				keyboard->SetPressed(static_cast<unsigned char>(wparam), false);
				keyboard->SetReleased(static_cast<unsigned char>(wparam), true);
			}
			break;
		}
	}
}

KR_END