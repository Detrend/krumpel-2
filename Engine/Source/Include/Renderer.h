// TODO: current rendering is not optimized
#pragma once

#include <glm/glm.hpp>

#include "Basics.h"
#include "Shader.h"

KR_BEGIN

class cCamera;
class cModel;
class iActor;
class cObject;
class cShader;
class cScene;

enum eRenderMode
{
    KR_RENDER_MODE_NORMAL = 0,          // renders triangles (default)
    KR_RENDER_MODE_WIREFRAME = 1,       // renders only wireframe of each triangle
    KR_RENDER_MODE_POINTS = 2           // renders only points of each triange vertex
};

enum eFaceCulling
{
    KR_FACE_CULLING_NONE = 0,           // no face culling
    KR_FACE_CULLING_BACK = 1,           // back faces (counter-clockwise order) are invisible
    KR_FACE_CULLING_FRONT = 2           // front faces (counter-clockwise order) are invisible
};

enum eTextureFiltering
{
    KR_TEXTURE_FILTERING_NEAREST = 0,
    KR_TEXTURE_FILTERING_LINEAR = 1
};

// Path to a material for rendering editor gizmos
constexpr const char* GizmoShaderPath = "Assets/Shaders/gizmo.krmat";

class cRenderer3D
{
private:
    eRenderMode			mRenderingMode;
    eFaceCulling		mFaceCullingMode;
    eTextureFiltering	mTextureFilteringMode;

    cShader				mGizmoShader;           // Gizmo shader used inside editor

public:
    cRenderer3D();
    // Sets new rendering mode
    void SetRenderingMode(eRenderMode Mode);
    // Sets new face culling mode
    void SetFaceCulling(eFaceCulling Mode);
    // Returns current rendering mode
    eRenderMode GetRenderingMode() const { return mRenderingMode; }
    // Returns current face culling mode of the renderer
    eFaceCulling GetFaceCulling() const { return mFaceCullingMode; }
    // Receives vertex array with vertices and indices bound to it and renders ElementCount elements.
    // Uses given shader for rendering.
    // Three matrices and uniforms have to be bound
    void Render(GLuint VertexArray,
        uint ElementCount,
        GLuint ShaderProgram,
        GLint Uniform1ID,
        const glm::mat4& Matrix1,
        GLint Uniform2ID,
        const glm::mat4& Matrix2,
        GLint Uniform3ID,
        const glm::mat4& Matrix3);
    // Renders a given object with given Camera. Applies the Transformation on the object before rendering it
    void RenderObject(const cObject& Object, const cCamera& Camera, const glm::mat4& Transformation);
    // Renders lines given by array of points in world space.
    // Translation is applied to every point of Lines.
    // When DoDepthTest is off the lines are visible even when obstructed.
    // When ScaleWithDistance is on the perspective is not applied to lines, therefore they remain the same size no
    // matter the distance from the camera.
    void RenderLines(const cCamera& Camera, const glm::mat4& Translation, 
                     glm::vec3 Color, const std::vector<glm::vec3>& Lines, 
                     bool DoDepthTest = true, int LineWidth = 2, bool ScaleWithDistance = false);
    // Renders given scene
    void Render(const cScene& Scene);
};

KR_END