#version 330 core
#extension GL_ARB_explicit_uniform_location : require   

out vec4 FragColor;

layout(location = 3) uniform vec4 uColorRGB;

void main()
{
	FragColor = vec4(uColorRGB.rgb, 1.0f);
}