#include <fstream>
#include <utility>

#include "Mesh.h"
#include "Basics.h"
#include "Generator.h"
#include "OpenGLError.h"
#include "Logger.h"
#include "Utility.h"

KR_BEGIN

cMesh::cMesh()
	: mVertexBufferData(), mIndexBufferData(), mVertexArrayHandle(), mVertexBufferHandle(), mIndexBufferHandle(), mInGpu(false)
{}

// move all data
cMesh::cMesh(cMesh&& aOther) noexcept
		: mInGpu(aOther.mInGpu),
		mVertexArrayHandle(aOther.mVertexArrayHandle),
		mVertexBufferHandle(aOther.mVertexBufferHandle),
		mIndexBufferHandle(aOther.mIndexBufferHandle),
		mVertexBufferData(std::move(aOther.mVertexBufferData)),
		mIndexBufferData(std::move(aOther.mIndexBufferData))
{
	aOther.mVertexArrayHandle = 0;
	aOther.mVertexBufferHandle = 0;
	aOther.mIndexBufferHandle = 0;
	aOther.mInGpu = false;
}

cMesh::~cMesh()
{
	if (InGpu()) {
		FreeFromGpu();
	}
}

cMesh& cMesh::operator=(cMesh&& aOther)
{
	if (this != &aOther) {
		mInGpu = aOther.mInGpu;
		aOther.mInGpu = false;

		mVertexBufferData = std::move(aOther.mVertexBufferData);
		mIndexBufferData = std::move(aOther.mIndexBufferData);

		mVertexArrayHandle = aOther.mVertexArrayHandle;
		aOther.mVertexArrayHandle = 0;
		mVertexBufferHandle = aOther.mVertexBufferHandle;
		aOther.mVertexBufferHandle = 0;
		mIndexBufferHandle = aOther.mIndexBufferHandle;
		aOther.mIndexBufferHandle = 0;
	}

	return *this;
}

void cMesh::UploadToGpu()
{
	if (!mInGpu) {
		cGenerator::GenVertexArray(mVertexArrayHandle);
		glBindVertexArray(mVertexArrayHandle);
		cGenerator::GenVertexBuffer(mVertexBufferHandle, mVertexBufferData.GetSize(), mVertexBufferData.Get());
		glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferHandle);
		cGenerator::GenIndexBuffer(mIndexBufferHandle, mIndexBufferData.GetCount(), mIndexBufferData.Get());
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBufferHandle);
		cGenerator::SetAttribute(mVertexArrayHandle, 0, 3, 5 * sizeof(float), 0);
		cGenerator::SetAttribute(mVertexArrayHandle, 1, 2, 5 * sizeof(float), 3 * sizeof(float));
		mInGpu = true;

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
}

void cMesh::FreeFromGpu()
{
	if (mInGpu) {
		krGlCall(glDeleteBuffers(1, &mVertexBufferHandle));
		krGlCall(glDeleteBuffers(1, &mIndexBufferHandle));
		krGlCall(glDeleteVertexArrays(1, &mVertexArrayHandle));
		mInGpu = false;
	}
}

void cMesh::SetVertexData(float * aCopyFrom, uint aSize)
{
	mVertexBufferData.Allocate(aCopyFrom, aSize);
}

void cMesh::SetIndexData(uint * aCopyFrom, uint aSize)
{
	mIndexBufferData.Allocate(aCopyFrom, aSize);
}

Result<> cMesh::LoadFromFile(const char* aFilePath)
{
	std::ifstream File;

	char Buffer64[8];
	char Buffer32[4];
	char Buffer160[20];
	int32 VertexCount;
	int32 IndexCount;

	struct VertexFormat {
		float Components[5];
	};
	VertexFormat Vertex;

	uint32 Index;

	File.open(aFilePath, std::ifstream::in | std::ifstream::binary);
	if (!File.is_open()) {
		// File does not exist
		return {eResult::RESULT_NOT_FOUND};
	}

	File.read(Buffer64, 8);
	if (!File) {
		// File is too short
		return {eResult::RESULT_UNEXPECTED};
	}

	// Check KR model signature
	bool Match = true;
	for (int i = 0; i < 8; i++) {
		if (Buffer64[i] != KR_MODEL_SIGNATURE[i]) {
			Match = false;
			break;
		}
	}

	if (Match == false) {
		// File is not a KR model
		return {eResult::RESULT_INVALID_ARGUMENT};
	}
	
	auto FlipArray = [](char* aArr, int aStart, int aEnd) -> void {
		for (int i = aStart, j = aEnd; i < j; i++, j--) {
			char Temp = aArr[i];
			aArr[i] = aArr[j];
			aArr[j] = Temp;
		}
	};

	// get number of vertices
	File.read(Buffer32, 4);
	if (!File) { KR_BREAKPOINT; }
	if (IsBigEndian()) {
		FlipArray(Buffer32, 0, 4);
	}
	VertexCount = *((int32*)Buffer32);

	// get number of indices
	File.read(Buffer32, 4);
	if (!File) { KR_BREAKPOINT; }
	if (IsBigEndian()) {
		FlipArray(Buffer32, 0, 4);
	}
	IndexCount = *((int32*)Buffer32);

	// Allocate storage for vertices
	mVertexBufferData.Allocate(VertexCount * sizeof(float) * 5);

	for (int v = 0; v < VertexCount; v++)
	{
		File.read(Buffer160, 20);
		if (!File) { KR_BREAKPOINT; }
		KR_ASSERT(!IsBigEndian());

		Vertex = *((VertexFormat*)Buffer160);
		for (int x = 0; x < 5; x++) {
			uint Index = v * 5 + x;
			mVertexBufferData[Index] = Vertex.Components[x];
		}
	}
	
	// Allocate storage for indices
	mIndexBufferData.Allocate(IndexCount * sizeof(uint32));

	for (int i = 0; i < IndexCount; i++)
	{
		File.read(Buffer32, 4);
		if (!File) { KR_BREAKPOINT; }
		KR_ASSERT(!IsBigEndian());

		Index = *((uint32*)Buffer32);
		mIndexBufferData[i] = Index;
	}

	return {eResult::RESULT_OK};
}

bool cMesh::InGpu() const
{
	return mInGpu;
}

bool cMesh::IsNull() const
{
	return (this == &NullMesh);
}

KR_END