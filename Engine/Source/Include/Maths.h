#pragma once

#include "Basics.h"
#include "glm/glm.hpp"

KR_BEGIN

namespace maths
{
    // Pair of two points
    struct Ray
    {
        glm::vec3 origin;
        glm::vec3 direction;
    };

    // Calculates distance of point from finite line
    // (the line is only between points 1 and 2 and does not expand to infinity)
    float DistanceFromFiniteLine2D(glm::vec2 Line1, glm::vec2 Line2, glm::vec2 Point);

    // Calculates a point in which the ray and infinite plane given by its point and normal intersect
    bool RayPlaneIntersects(Ray TheRay, Ray Plane, glm::vec3& IntersectPoint);
}

KR_END
