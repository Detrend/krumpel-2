#
#       Krumpel model exporter
#       Converts .obj model format to krumpel model format, which is just a heap of bytes
#
#       Signature: "kr model" in 8 bytes. Two 32 ints follow - vertices & indices count
#
#       Vertex format is: PosX, PosY, PosZ, TexX, TexY all in float32
#       Index format is:  IndexX as uint32
#
#       Uses little endian
#

import numpy as np

def export_log(output_log_path, vertices, indices):
    with open(output_log_path, "w") as krmodel:
        print("VERTICES", file=krmodel)
        print(vertices, file=krmodel)
        print("INDICES", file=krmodel)
        print(indices, file=krmodel)


def export(output_file_path, vertices, indices):
    with open(output_file_path, "wb") as output:

        sign = "kr model"
        bytesign = [ord(x) for x in sign]
        output.write(bytearray(bytesign))

        vertex_count = len(vertices)
        index_count = len(indices)

        print("Exporting", vertex_count, "vertices and", index_count, "indices to", output_file_path)

        output.write(np.int32(vertex_count).tobytes())
        output.write(np.int32(index_count).tobytes())

        for vertex in vertices:
            for el in vertex:
                conv = np.float32(el)
                output.write(conv.tobytes())

        for index in indices:
            conv = np.uint32(index)
            output.write(conv.tobytes())

def move_elements(raw_vertices, raw_uvs, raw_faces, output_file):
    vertices = []
    indices = []
    faces = []

    for face in raw_faces:
        if len(face) == 3:
            faces.append(face)
        if len(face) == 4:
            first_triangle = [face[0], face[1], face[2]]
            second_triangle = [face[2], face[3], face[0]]
            faces.append(first_triangle)
            faces.append(second_triangle)
        # else:
        #     raise("This number of faces is not implemented!")
        
    for face in faces:
        for point in face:
            vertex_index = point[0]
            uv_index = point[1]

            new_index = len(indices)
            new_vertex = raw_vertices[vertex_index] + raw_uvs[uv_index]

            vertices.append(new_vertex)
            indices.append(new_index)

    export(output_file, vertices, indices)



def parse_file(input_file, output_file):

    vertices_list = [None]
    uvs_list = [None]       #texture uvs
    face_list = []

    with open(input_file, "r") as objmodel:
        for line in objmodel:
            line_split = line.split()

            if len(line_split) > 0:

                if line_split[0] == 'v':
                    if len(line_split) in [4, 5]:
                        vertex_set = []

                        for v in line_split[1:]:
                            val = float(v)
                            vertex_set.append(val)
                        vertices_list.append(vertex_set[:3])

                elif line_split[0] == 'vt':
                    if len(line_split) in [3, 4]:
                        uv_set = []

                        for v in line_split[1:]:
                            val = float(v)
                            uv_set.append(val)
                        uvs_list.append(uv_set[:2])

                elif line_split[0] == 'f':
                    if len(line_split) in [4, 5, 6]:
                        face_set = []

                        for f in line_split[1:]:
                            element_split = f.split('/')
                            element_set = []
                            for el in element_split:
                                if el != '':
                                    element_set.append(int(el))
                            
                            face_set.append(element_set[:2])

                        face_list.append(face_set)

    move_elements(vertices_list, uvs_list, face_list, output_file)

models = ["crytek_sponza_huge_vray_bricks.txt", 
        "crytek_sponza_huge_vray_ceiling.txt",
        "crytek_sponza_huge_vray_chain.txt",
        "crytek_sponza_huge_vray_column_a.txt",
        "crytek_sponza_huge_vray_column_b.txt",
        "crytek_sponza_huge_vray_column_c.txt",
        "crytek_sponza_huge_vray_details.txt",
        "crytek_sponza_huge_vray_fabric_a.txt",
        "crytek_sponza_huge_vray_fabric_c.txt",
        "crytek_sponza_huge_vray_fabric_d.txt",
        "crytek_sponza_huge_vray_fabric_e.txt",
        "crytek_sponza_huge_vray_fabric_f.txt",
        "crytek_sponza_huge_vray_fabric_g.txt",
        "crytek_sponza_huge_vray_flagpole.txt",
        "crytek_sponza_huge_vray_floor.txt",
        "crytek_sponza_huge_vray_leaf.txt",
        "crytek_sponza_huge_vray_Material__47.txt",
        "crytek_sponza_huge_vray_Material__57.txt",
        "crytek_sponza_huge_vray_Material__298.txt",
        "crytek_sponza_huge_vray_roof.txt",
        "crytek_sponza_huge_vray_vase.txt",
        "crytek_sponza_huge_vray_vase_hanging.txt",
        "crytek_sponza_huge_vray_vase_round.txt"]

def run():
    print(""""path to .obj file" "path to output" """)
    # files = input().split()

    # parse_file(files[0], files[1])
    for model_name in models:
        parse_file(model_name + ".obj", model_name + ".krmod")

run()